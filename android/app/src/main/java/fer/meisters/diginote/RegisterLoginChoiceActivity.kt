package fer.meisters.diginote

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import fer.meisters.diginote.databinding.ActivityRegisterLoginChoiceBinding

class RegisterLoginChoiceActivity : AppCompatActivity() {

    companion object {
        const val TAG = "RegisterLoginActivity"
    }

    private lateinit var binding: ActivityRegisterLoginChoiceBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRegisterLoginChoiceBinding.inflate(layoutInflater)
        setContentView(binding.root)

        Log.i(TAG, "Activity created.")

        binding.goToRegister.setOnClickListener {
            startActivity(Intent(this, RegisterActivity::class.java))
        }

        binding.goToLogin.setOnClickListener {
            startActivity(Intent(this, LoginActivity::class.java))
        }
    }
}