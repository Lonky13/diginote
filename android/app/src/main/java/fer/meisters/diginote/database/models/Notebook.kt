package fer.meisters.diginote.database.models

import org.jetbrains.exposed.dao.UUIDEntity
import org.jetbrains.exposed.dao.UUIDEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.UUIDTable
import java.util.*

object Notebooks : UUIDTable() {
    val userUUID = uuid("user_uuid")
    val name = varchar("name", 25)
    val originalUUID = uuid("original_uuid").nullable()
    val public = bool("public")
    val directoryUUID = uuid("directory_uuid")
}

class Notebook(id: EntityID<UUID>) : UUIDEntity(id) {
    companion object : UUIDEntityClass<Notebook>(Notebooks)

    var userUUID by Notebooks.userUUID
    var name by Notebooks.name
    var originalUUID by Notebooks.originalUUID
    var public by Notebooks.public
    var directoryUUID by Notebooks.directoryUUID
}