package fer.meisters.diginote.repositories

import android.util.Log
import fer.meisters.diginote.BuildConfig
import fer.meisters.diginote.api.APIService
import fer.meisters.diginote.database.db_connection
import fer.meisters.diginote.database.models.Notebook
import fer.meisters.diginote.models.FileTree
import fer.meisters.diginote.models.NotebookModel
import fer.meisters.diginote.models.PageModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.jetbrains.exposed.sql.transactions.transaction
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.util.*


object NotebookRepository {
    private val retrofit: Retrofit = Retrofit.Builder()
        .baseUrl("http://" + BuildConfig.DIGINOTE_DB_HOST + ":3000")
        .addConverterFactory(ScalarsConverterFactory.create())
        .addConverterFactory(
            GsonConverterFactory.create()
        )
        .build()

    private val service = retrofit.create(APIService::class.java)

    suspend fun fetchContent(notebookUUID: UUID, token: String): NotebookModel {
        return withContext(Dispatchers.IO) {
            service.getNotebookContent(token, notebookUUID).body()!!
        }
    }

    suspend fun updateContent(notebookUUID: UUID, content: NotebookModel, token: String) {
        withContext(Dispatchers.IO) {
            service.updateNotebookContent(token, notebookUUID, content)
        }
    }

    suspend fun OCR(notebookUUID: UUID, position: Int, token: String): String {
        return withContext(Dispatchers.IO) {
            service.requestOCR(token, notebookUUID, position).body()!!
        }
    }

    suspend fun Runner(notebookUUID: UUID, position: Int, token: String): String {
        return withContext(Dispatchers.IO) {
            service.requestRunner(token, notebookUUID, position).body()!!
        }
    }

    suspend fun merge(
        notebookUUIDs: List<UUID>,
        userUUID: UUID,
        directoryUUID: UUID,
        directory: FileTree.Node?,
        token: String
    ) {
        withContext(Dispatchers.IO) {
            val mergedNotebook = NotebookModel(mutableListOf(), mutableListOf())
            var position = 0
            for (uuid in notebookUUIDs) {
                val content = service.getNotebookContent(token, uuid).body()!!
                val pages = content.pages
                val hashes = content.hashes
                for (i in 0 until pages.size) {
                    if (!mergedNotebook.hashes.contains(hashes[i])) {
                        mergedNotebook.pages.add(
                            PageModel(
                                position,
                                pages[i].textPath.toMutableList(),
                                pages[i].code, // Copy needed?
                                pages[i].drawingPath.toMutableList(),
                                pages[i].color,
                                pages[i].grid
                            )
                        )
                        mergedNotebook.hashes.add(hashes[i])
                        position += 1
                    }
                }
            }

            val addedNotebook = transaction(db_connection()) {
                Notebook.new {
                    this.directoryUUID = directoryUUID
                    this.userUUID = userUUID
                    this.name = "<merged notebook>"
                    this.public = false
                }
            }

            directory?.notebooks?.add(
                FileTree.Node(
                    directory,
                    mutableListOf(),
                    mutableListOf(),
                    null,
                    addedNotebook
                )
            )

            updateContent(addedNotebook.id.value, mergedNotebook, token)
        }
    }

    // Implement delete
    suspend fun deleteNotebook() {

    }
}