package fer.meisters.diginote

import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.core.app.ActivityOptionsCompat
import fer.meisters.diginote.database.db_connection
import fer.meisters.diginote.database.db_scope
import fer.meisters.diginote.database.models.*
import fer.meisters.diginote.databinding.ActivityLauncherBinding
import kotlinx.coroutines.*
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.StdOutSqlLogger
import org.jetbrains.exposed.sql.addLogger
import org.jetbrains.exposed.sql.transactions.transaction

class LauncherActivity : AppCompatActivity() {

    companion object {
        const val TAG = "LauncherActivity"
    }

    private lateinit var binding: ActivityLauncherBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLauncherBinding.inflate(layoutInflater)
        setContentView(binding.root)

        Log.i(TAG, "Activity created.")

        db_connection()

        db_scope.launch {
            transaction {
                addLogger(StdOutSqlLogger)

                SchemaUtils.create(Users, Notebooks, Directories)
            }

            withContext(Dispatchers.Main) {
                val user: SharedPreferences = getSharedPreferences("user", MODE_PRIVATE)

                if (user.contains("jwt_token")) {
                    startActivity(Intent(this@LauncherActivity, HomeActivity::class.java))
                } else {

                    val intent = Intent(this@LauncherActivity, RegisterLoginChoiceActivity::class.java)
                    val options = ActivityOptionsCompat.makeSceneTransitionAnimation(this@LauncherActivity, binding.splashIcon, "splash_transition")
                    startActivity(intent, options.toBundle())
                }

                finish()
            }
        }
    }
}
