package fer.meisters.diginote.repositories

import fer.meisters.diginote.database.db_connection
import fer.meisters.diginote.database.models.Directories
import fer.meisters.diginote.database.models.Notebooks
import fer.meisters.diginote.database.models.User
import fer.meisters.diginote.database.models.Users
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.transactions.transaction
import org.jetbrains.exposed.sql.update
import org.mindrot.jbcrypt.BCrypt
import java.util.*

object UserRepository {

    suspend fun emailExists(email: String): Boolean {
        return withContext(Dispatchers.IO) {
            transaction(db_connection()) {
                !User.find { Users.email eq email }.empty()
            }
        }
    }

    suspend fun usernameExists(username: String): Boolean {
        return withContext(Dispatchers.IO) {
            transaction(db_connection()) {
                !User.find { Users.username eq username }.empty()
            }
        }
    }

    suspend fun updateUserInfo(
        userUUID: UUID,
        username: String?,
        email: String?,
        password: String?
    ) {
        withContext(Dispatchers.IO) {
            transaction(db_connection()) {
                Users.update({ Users.id eq userUUID }) {
                    if (username != null) it[Users.username] = username
                    if (email != null) it[Users.email] = email
                    if (password != null) it[Users.hashedPassword] =
                        BCrypt.hashpw(password, BCrypt.gensalt())
                }
            }
        }
    }

    suspend fun deleteUser(userUUID: UUID) {
        withContext(Dispatchers.IO) {
            transaction(db_connection()) {
                Notebooks.deleteWhere { Notebooks.userUUID eq userUUID }
                Directories.deleteWhere { Directories.userUUID eq userUUID }
                Users.deleteWhere { Users.id eq userUUID }
            }
        }
    }
}