package fer.meisters.diginote.models

import com.google.gson.annotations.SerializedName

data class NotebookModel (
    @SerializedName("pages") val pages: MutableList<PageModel>,
    @SerializedName("hashes") val hashes: MutableList<String>
)