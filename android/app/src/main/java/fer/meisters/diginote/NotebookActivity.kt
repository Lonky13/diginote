package fer.meisters.diginote

import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.compose.foundation.ScrollableRow
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material.icons.filled.NoteAdd
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.setContent
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.AmbientContext
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.viewModel
import fer.meisters.diginote.ui.AppTheme
import fer.meisters.diginote.viewmodels.HomeViewModel
import fer.meisters.diginote.viewmodels.NotebookViewModel
import java.io.File
import java.util.*

class NotebookActivity : AppCompatActivity() {

    companion object {
        const val TAG = "NotebookActivity"
    }

    private val viewModel: NotebookViewModel by viewModels()

    private lateinit var notebookUUID: UUID
    private lateinit var token: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        notebookUUID = UUID.fromString(intent.getStringExtra("notebookUUID"))
        val userPreferences: SharedPreferences = getSharedPreferences("user", MODE_PRIVATE)
        token = userPreferences.getString("jwt_token", "This user does not exist!")!!
        val file = File(this.cacheDir, notebookUUID.toString())

        setContent {
            AppTheme {

                Scaffold(
                    floatingActionButton = {
                        val selectionMode by viewModel.selectionMode.observeAsState(false)

                        if (!selectionMode) {
                            FloatingActionButton(onClick = {
                                viewModel.addPage(
                                    notebookUUID,
                                    token,
                                    file
                                )
                            }) {
                                Icon(imageVector = Icons.Filled.NoteAdd, tint = Color.White)
                            }
                        } else {
                            FloatingActionButton(
                                backgroundColor = MaterialTheme.colors.primary,
                                onClick = {
                                    viewModel.deletePages(notebookUUID, token, file)
                                }) {
                                Icon(imageVector = Icons.Filled.Delete, tint = Color.White)
                            }
                        }
                    },
                    bodyContent = {
                        ScrollableRow(
                            modifier = Modifier
                                .fillMaxSize()
                                .padding(10.dp),
                            horizontalArrangement = Arrangement.spacedBy(10.dp),
                            verticalAlignment = Alignment.CenterVertically
                        ) {

                            val notebook by viewModel.notebook.observeAsState(initial = null)
                            val selectedPages by viewModel.selectedPages.observeAsState(listOf())

                            if (notebook != null) {
                                for (page in notebook!!.pages) {
                                    Page(
                                        page.position + 1,
                                        notebook!!.pages.size,
                                        notebookUUID,
                                        selectedPages.contains(page.position)
                                    )
                                }
                            }
                        }
                    })
            }
        }

        viewModel.fetchContent(notebookUUID, token, file)
    }
}

@Composable
fun Page(
    position: Int,
    pages: Int,
    notebookUUID: UUID,
    selected: Boolean,
    viewModel: NotebookViewModel = viewModel()
) {
    val context = AmbientContext.current

    val selectionMode by viewModel.selectionMode.observeAsState(false)

    Column(
        verticalArrangement = Arrangement.SpaceEvenly,
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier
            .background(if (selected) MaterialTheme.colors.surface else MaterialTheme.colors.background)
            .clickable(onClick = {
                if (!selectionMode) {
                    val intent = Intent(context, PageActivity::class.java)
                    intent.putExtra("notebookUUID", notebookUUID.toString())
                    intent.putExtra("position", position - 1)
                    context.startActivity(intent)
                } else {
                    if (selected) {
                        viewModel.removePageFromSelection(position - 1)
                    } else {
                        viewModel.addPageToSelection(position - 1)
                    }
                }
            },
                onLongClick = {
                    if (!selectionMode) {
                        viewModel.enableSelectionMode()
                        viewModel.addPageToSelection(position - 1)
                    }
                })
    ) {
        Box(
            modifier = Modifier
                .weight(0.9f)
                .aspectRatio(0.7071f, true)
                .background(Color.White)
        ) {}
        Text(text = "$position/$pages", color = Color.White, modifier = Modifier.padding(20.dp))
    }

}