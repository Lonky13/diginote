package fer.meisters.diginote.repositories

import android.util.Log
import fer.meisters.diginote.database.db_connection
import fer.meisters.diginote.database.models.Notebook
import fer.meisters.diginote.database.models.Notebooks
import fer.meisters.diginote.database.models.User
import fer.meisters.diginote.database.models.Users
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.innerJoin
import org.jetbrains.exposed.sql.or
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.transactions.transaction

object SearchRepository {
    suspend fun searchNotebooks(query: String, limit: Int): Pair<List<User>, List<Notebook>> {
        return withContext(Dispatchers.IO) {
            transaction(db_connection()) {
                val rows = Notebooks.innerJoin(Users, { userUUID }, { Users.id })
                    .select { (Notebooks.name like "%$query%" or (Users.username like "%$query%")) and (Notebooks.public eq true) }
                    .limit(limit)

                Pair(User.wrapRows(rows).toList(), Notebook.wrapRows(rows).toList())
            }
        }
    }
}