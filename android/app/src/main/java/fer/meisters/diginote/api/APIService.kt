package fer.meisters.diginote.api

import fer.meisters.diginote.models.NotebookModel
import fer.meisters.diginote.models.PageModel
import retrofit2.Response
import retrofit2.http.*
import java.util.*

interface APIService {
    @GET("/api/{token}/notebook/{uuid}")
    suspend fun getNotebookContent(
        @Path("token") token: String,
        @Path("uuid") uuid: UUID
    ): Response<NotebookModel>

    @PUT("/api/{token}/notebook/{uuid}")
    suspend fun updateNotebookContent(
        @Path("token") token: String,
        @Path("uuid") uuid: UUID,
        @Body content: NotebookModel
    ): Response<Unit>

    @GET("/api/{token}/ocr/notebook/{uuid}/position/{position}")
    suspend fun requestOCR(
        @Path("token") token: String,
        @Path("uuid") uuid: UUID,
        @Path("position") position: Int
    ): Response<String>

    @GET("/api/{token}/runner/notebook/{uuid}/position/{position}")
    suspend fun requestRunner(
        @Path("token") token: String,
        @Path("uuid") uuid: UUID,
        @Path("position") position: Int
    ): Response<String>
}