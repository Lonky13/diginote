package fer.meisters.diginote.viewmodels

import android.app.Application
import android.content.SharedPreferences
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import fer.meisters.diginote.BuildConfig
import fer.meisters.diginote.R
import fer.meisters.diginote.isValidEmail
import fer.meisters.diginote.models.UserError
import fer.meisters.diginote.repositories.UserRepository
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.util.*

class ProfileViewModel(application: Application) : AndroidViewModel(application) {
    val editMode: LiveData<Boolean> get() = mutableEditMode
    val askingConfirmation: LiveData<Boolean> get() = mutableAskingConfirmation
    val errors: LiveData<List<UserError>> get() = mutableErrors

    private val mutableEditMode = MutableLiveData<Boolean>(false)
    private val mutableAskingConfirmation = MutableLiveData<Boolean>(false)
    private val mutableErrors = MutableLiveData<List<UserError>>(listOf())

    lateinit var username: String
    lateinit var email: String

    lateinit var newUsername: String
    lateinit var newEmail: String
    var newPassword: String = ""
    var confirmPassword: String = ""

    fun setInfo(username: String, email: String) {
        this.username = username
        this.email = email

        this.newUsername = username
        this.newEmail = email
    }

    fun exitEditMode() {
        mutableEditMode.value = false
        mutableErrors.value = emptyList()
    }

    fun enterEditMode() {
        mutableEditMode.value = true

        newUsername = username
        newEmail = email
        newPassword = ""
        confirmPassword = ""
    }

    fun saveChanges(userUUID: UUID) {
        GlobalScope.launch {
            val errorsList = mutableListOf<UserError>()

            if (newPassword != "") {
                if (newPassword.contains(Regex("\\s"))) {
                    errorsList.add(UserError.PASSWORD_NO_SPACES)
                } else if (newPassword.length < 8) {
                    errorsList.add(UserError.PASSWORD_MIN_LENGTH)
                } else if (confirmPassword != newPassword) {
                    errorsList.add(UserError.PASSWORDS_DO_NOT_MATCH)
                }
            }

            if (newEmail != email) {
                if (!newEmail.isValidEmail()) {
                    errorsList.add(UserError.INVALID_EMAIL)
                } else if (newEmail.length > 25) {
                    errorsList.add(UserError.EMAIL_MAX_LENGTH)
                } else if (UserRepository.emailExists(newEmail)) {
                    errorsList.add( UserError.EMAIL_EXISTS)
                }
            }

            if (newUsername != username) {
                if (newUsername.contains(Regex("\\s"))) {
                    errorsList.add(UserError.USERNAME_NO_SPACES)
                } else if (newUsername.length < 3) {
                    errorsList.add(UserError.USERNAME_MIN_LENGTH)
                } else if (newUsername.isValidEmail()) {
                    errorsList.add(UserError.USERNAME_CAN_NOT_BE_EMAIL)
                } else if (newUsername.length > 25) {
                    errorsList.add(UserError.USERNAME_MAX_LENGTH)
                } else if (UserRepository.usernameExists(newUsername)) {
                    errorsList.add(UserError.USERNAME_EXISTS)
                }
            }

            if (newUsername == username && newEmail == email && newPassword == ""){
                mutableErrors.postValue(mutableListOf<UserError>())
                mutableEditMode.postValue(false)
                return@launch
            }

            if (errorsList.isEmpty()) {
                mutableErrors.postValue(mutableListOf())

                UserRepository.updateUserInfo(
                    userUUID = userUUID,
                    username = if (newUsername != username) newUsername else null,
                    email = if (newEmail != email) newEmail else null,
                    password = if (newPassword != "") newPassword else null
                )

                username = newUsername
                email = newEmail

                val userPreferences: SharedPreferences =
                    getApplication<Application>().applicationContext.getSharedPreferences(
                        "user",
                        AppCompatActivity.MODE_PRIVATE
                    )
                val userInfo = Jwts.parser().setSigningKey(BuildConfig.JWT_SECRET.toByteArray())
                    .parseClaimsJws(
                        userPreferences.getString("jwt_token", null)
                    ).body

                val jwtToken: String =
                    Jwts.builder().claim("uuid", userUUID)
                        .claim("username", username)
                        .claim("email", email)
                        .claim("role", userInfo["role"] as String)
                        .claim("rootUUID", userInfo["rootUUID"] as String).signWith(
                            SignatureAlgorithm.HS256,
                            BuildConfig.JWT_SECRET.toByteArray()
                        ).compact()

                val editUser: SharedPreferences.Editor = userPreferences.edit()
                editUser.putString("jwt_token", jwtToken)
                editUser.apply()
            
                mutableEditMode.postValue(false)
            } else {
                mutableErrors.postValue(errorsList)
            }
        }
    }

    fun deleteProfile(userUUID: UUID) {
        GlobalScope.launch { 
            UserRepository.deleteUser(userUUID)
        }
    }

    fun askConfirmation() {
        mutableAskingConfirmation.value = true
    }

    fun acceptConfirmation(userUUID: UUID){
        deleteProfile(userUUID)
        mutableAskingConfirmation.value = false
    }

    fun declineConfirmation(){
        mutableAskingConfirmation.value = false
    }
}