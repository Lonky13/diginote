package fer.meisters.diginote

import fer.meisters.diginote.fragments.RegisterUsername
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.fragment.NavHostFragment.findNavController
import com.aceinteract.android.stepper.StepperNavListener
import fer.meisters.diginote.database.db_connection
import fer.meisters.diginote.database.db_scope
import fer.meisters.diginote.database.models.Directory
import fer.meisters.diginote.database.models.User
import fer.meisters.diginote.database.models.Users
import fer.meisters.diginote.databinding.ActivityRegisterBinding
import fer.meisters.diginote.databinding.FragmentRegisterEmailBinding
import fer.meisters.diginote.databinding.FragmentRegisterUsernameBinding
import fer.meisters.diginote.fragments.RegisterEmail
import fer.meisters.diginote.fragments.RegisterPassword
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import kotlinx.coroutines.*
import org.jetbrains.exposed.sql.transactions.transaction
import org.mindrot.jbcrypt.BCrypt

class RegisterActivity : AppCompatActivity(), StepperNavListener, View.OnClickListener,
    RegisterUsername.OnUsernameChangedListener, RegisterEmail.OnEmailChangedListener,
    RegisterPassword.OnPasswordChangedListener, RegisterPassword.OnPasswordConfirmChangedListener {

    companion object {
        const val TAG = "RegisterActivity"
    }

    private var username: String? = null
    private var email: String? = null
    private var password: String? = null
    private var passwordConfirm: String? = null

    private var currentJob: Job? = null

    private lateinit var binding: ActivityRegisterBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRegisterBinding.inflate(layoutInflater)
        setContentView(binding.root)

        Log.i(TAG, "Activity created.")

        db_connection()

        binding.stepper.setupWithNavController(findNavController(supportFragmentManager.findFragmentById(R.id.register_frame_stepper)!!))

        binding.stepper.stepperNavListener = this
        binding.registerNext.setOnClickListener(this)
    }

    override fun onClick(view: View?) {
        when (view) {
            binding.registerNext -> {
                binding.stepper.goToNextStep()
            }
        }
    }

    override fun onPasswordChanged(password: String): String? {
        binding.registerNext.isEnabled = false
        return when {
            password.length < 8 -> getString(R.string.password_min_length)
            password.contains(Regex("\\s")) -> getString(R.string.password_no_spaces)
            password != passwordConfirm -> {
                this.password = password
                null
            }
            else -> {
                this.password = password
                binding.registerNext.isEnabled = true
                null
            }
        }
    }

    override fun onPasswordConfirmChanged(password_confirm: String): String? {
        binding.registerNext.isEnabled = false
        return if (password != password_confirm) {
            getString(R.string.passwords_do_not_match)
        } else {
            binding.registerNext.isEnabled = true
            this.passwordConfirm = password_confirm
            null
        }
    }

    override fun onEmailChanged(email: String, frag_binding: FragmentRegisterEmailBinding) {
        currentJob?.cancel()
        binding.registerNext.isEnabled = false
        if (email.length > 25) {
            frag_binding.registerEmail.error = getString(R.string.email_max_length)
        } else if (!email.isValidEmail()) {
            frag_binding.registerEmail.error = getString(R.string.invalid_email)
        } else {
            currentJob = db_scope.launch {
                val result = withContext(Dispatchers.IO) {
                    val users = transaction {
                        User.find { Users.email eq email }.toList()
                    }

                    if (users.isNotEmpty()) getString(R.string.email_exists) else null
                }

                withContext(Dispatchers.Main) {
                    if (result == null) {
                        binding.registerNext.isEnabled = true
                        this@RegisterActivity.email = email
                    }

                    frag_binding.registerEmail.error = result
                }
            }
        }
    }

    override fun onUsernameChanged(username: String, frag_binding: FragmentRegisterUsernameBinding) {
        currentJob?.cancel()
        binding.registerNext.isEnabled = false
        when {
            username.length < 3 -> frag_binding.registerUsername.error =
                getString(R.string.username_min_length)
            username.length > 25 -> frag_binding.registerUsername.error =
                getString(R.string.username_max_length)
            username.isValidEmail() -> frag_binding.registerUsername.error =
                getString(R.string.username_can_not_be_email)
            username.contains(Regex("\\s")) -> frag_binding.registerUsername.error =
                getString(R.string.username_no_spaces)
            else -> {
                currentJob = db_scope.launch {
                    val result = withContext(Dispatchers.IO) {
                        val users = transaction {
                            User.find { Users.username eq username }.toList()
                        }

                        if (users.isNotEmpty()) getString(R.string.username_exists) else null
                    }

                    withContext(Dispatchers.Main) {
                        if (result == null) {
                            binding.registerNext.isEnabled = true
                            this@RegisterActivity.username = username
                        }

                        frag_binding.registerUsername.error = result
                    }
                }
            }
        }
    }

    override fun onStepChanged(step: Int) {
        binding.registerNext.isEnabled = false
        when (step) {
            0 -> {
                username = null
                email = null
                password = null
            }
            1 -> {
                email = null
                password = null
            }
            2 -> {
                password = null
            }
        }
    }

    override fun onCompleted() {
        binding.registerNext.isEnabled = false

        db_scope.launch {
            val user = transaction {
                User.new {
                    val id = this.id
                    username = this@RegisterActivity.username!!
                    email = this@RegisterActivity.email!!
                    role = Users.Role.User
                    hashedPassword = BCrypt.hashpw(this@RegisterActivity.password!!, BCrypt.gensalt())
                    rootUUID = Directory.new { this.userUUID = id.value}.id.value
                }
            }

            withContext(Dispatchers.Main) {
                val jwtToken: String =
                    Jwts.builder().claim("uuid", user.id.toString())
                        .claim("username", user.username)
                        .claim("email", user.email)
                        .claim("role", Users.Role.User)
                        .claim("rootUUID", user.rootUUID.toString()).signWith(
                            SignatureAlgorithm.HS256,
                            BuildConfig.JWT_SECRET.toByteArray()
                        ).compact()

                val user: SharedPreferences =
                    getSharedPreferences("user", MODE_PRIVATE)
                val editUser: SharedPreferences.Editor = user.edit()
                editUser.putString("jwt_token", jwtToken)
                editUser.apply()

                startActivity(Intent(this@RegisterActivity, HomeActivity::class.java))
                finishAffinity()
            }
        }
    }

    override fun onBackPressed() {
        if (binding.stepper.currentStep == 0) {
            super.onBackPressed()
        } else {
            findNavController(supportFragmentManager.findFragmentById(R.id.register_frame_stepper)!!).navigateUp()
        }
    }
}