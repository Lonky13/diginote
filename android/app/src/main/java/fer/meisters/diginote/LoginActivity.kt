package fer.meisters.diginote

import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import fer.meisters.diginote.database.db_connection
import fer.meisters.diginote.database.db_scope
import fer.meisters.diginote.database.models.User
import fer.meisters.diginote.database.models.Users
import fer.meisters.diginote.databinding.ActivityLoginBinding
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.jetbrains.exposed.sql.transactions.transaction
import org.mindrot.jbcrypt.BCrypt

class LoginActivity : AppCompatActivity(), View.OnClickListener {

    companion object {
        const val TAG = "LoginActivity"
    }

    private val activityContext = this

    private lateinit var binding: ActivityLoginBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        Log.i(TAG, "Activity created.")

        db_connection()

        binding.login.setOnClickListener(this)
    }

    override fun onClick(view: View?) {
        when (view) {
            binding.login -> {
                binding.login.isEnabled = false
                Log.i(TAG, "Login clicked.")

                val usernameEmail = binding.usernameEmailLogin.editText?.text.toString()
                val password = binding.passwordLogin.editText?.text.toString()

                db_scope.launch {
                    val users = if (usernameEmail.isValidEmail()) {
                        transaction {
                            User.find { Users.email eq usernameEmail }.toList()
                        }
                    } else {
                        transaction {
                            User.find { Users.username eq usernameEmail }.toList()
                        }
                    }

                    withContext(Dispatchers.Main) {
                        binding.passwordLogin.error = null
                        if (users.isEmpty()) {
                            binding.usernameEmailLogin.error = if (usernameEmail.isValidEmail()) {
                                getString(R.string.wrong_email)
                            } else {
                                getString(R.string.wrong_username)
                            }
                        } else {
                            binding.usernameEmailLogin.error = null

                            if (BCrypt.checkpw(password, users.first().hashedPassword)) {
                                val jwtToken: String =
                                    Jwts.builder().claim("uuid", users.first().id.toString())
                                        .claim("username", users.first().username)
                                        .claim("email", users.first().email)
                                        .claim("role", users.first().role.toString())
                                        .claim("rootUUID", users.first().rootUUID.toString()).signWith(
                                            SignatureAlgorithm.HS256,
                                            BuildConfig.JWT_SECRET.toByteArray()
                                        ).compact()

                                val user: SharedPreferences =
                                    getSharedPreferences("user", MODE_PRIVATE)
                                val editUser: SharedPreferences.Editor = user.edit()
                                editUser.putString("jwt_token", jwtToken)
                                editUser.apply()

                                startActivity(Intent(activityContext, HomeActivity::class.java))
                                finishAffinity()
                            } else {
                                binding.passwordLogin.error = getString(R.string.wrong_password)
                            }
                        }

                        binding.login.isEnabled = true
                    }
                }
            }
        }
    }
}