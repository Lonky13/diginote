package fer.meisters.diginote.ui

import androidx.compose.material.MaterialTheme
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color

@Composable
fun AppTheme(content: @Composable () -> Unit) {
    val colors = lightColors(
        primary = Color(0xffbf180b),
        primaryVariant = Color(0xff870000),
        onPrimary = Color(0xffffffff),

        secondary = Color(0xff2f4858),
        secondaryVariant = Color(0xff5a7385),
        onSecondary = Color(0xffffffff),

        background = Color(0xff130d0d),
        onBackground = Color.White,

        surface = Color(0xff676ee9),
        onSurface = Color(0xffffffff),

        error = Color(0xffFEC0CE),
        onError = Color(0xff000000)

    // 3772ff
    )

    MaterialTheme(content = content, colors = colors)
}