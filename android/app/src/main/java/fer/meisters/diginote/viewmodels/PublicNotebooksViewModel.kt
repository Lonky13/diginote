package fer.meisters.diginote.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import fer.meisters.diginote.database.models.Notebook
import fer.meisters.diginote.database.models.User
import fer.meisters.diginote.repositories.FileTreeRepository
import fer.meisters.diginote.repositories.SearchRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.util.*

class PublicNotebooksViewModel : ViewModel() {
    val searchResults: LiveData<List<Pair<User, Notebook>>> get() = mutableSearchResults
    val searchMode: LiveData<Boolean> get() = mutableSearchMode

    private val mutableSearchResults = MutableLiveData<List<Pair<User, Notebook>>>(null)
    private val mutableSearchMode = MutableLiveData<Boolean>(false)

    fun search(query: String) {
        if(query.isBlank()){
            mutableSearchResults.value = null
            return
        }

        GlobalScope.launch(Dispatchers.Main) {
            val result = SearchRepository.searchNotebooks(query, 20)

            mutableSearchResults.value = result.first.zip(result.second)
        }
    }
    
    fun downloadNotebook(notebook: Notebook, rootUUID: UUID){
        GlobalScope.launch(Dispatchers.Main) {
            FileTreeRepository.downloadNotebook(notebook, rootUUID)
        }
    }
    
    fun enterSearchMode(){
        mutableSearchMode.value = true
    }

    fun exitSearchMode(){
        mutableSearchMode.value = false
        mutableSearchResults.value = null
    }
}