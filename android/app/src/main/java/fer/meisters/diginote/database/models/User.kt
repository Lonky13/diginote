package fer.meisters.diginote.database.models

import org.jetbrains.exposed.dao.UUIDEntity
import org.jetbrains.exposed.dao.UUIDEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.UUIDTable
import java.util.*

object Users : UUIDTable() {
    val username = varchar("username", 25)
    val email = varchar("email", 25)
    val role = enumeration("role", Role::class)
    val hashedPassword = varchar("hashed_password", 60)
    val rootUUID = uuid("root_uuid")

    enum class Role(val status: Int) {
        User(0),
        Admin(1),
        Maintainer(2)
    }
}

class User(id: EntityID<UUID>) : UUIDEntity(id) {
    companion object : UUIDEntityClass<User>(Users)

    var username by Users.username
    var email by Users.email
    var role by Users.role
    var hashedPassword by Users.hashedPassword
    var rootUUID by Users.rootUUID
}