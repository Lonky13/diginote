package fer.meisters.diginote.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import fer.meisters.diginote.database.models.Directory
import fer.meisters.diginote.database.models.Notebook
import fer.meisters.diginote.models.FileTree
import fer.meisters.diginote.repositories.FileTreeRepository
import fer.meisters.diginote.repositories.NotebookRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.util.*

class HomeViewModel : ViewModel() {
    val directories: LiveData<List<Directory>> get() = mutableDirectories
    val notebooks: LiveData<List<Notebook>> get() = mutableNotebooks
    val addMenuActive: LiveData<Boolean> get() = mutableAddMenuActive

    val directoryCreationMode: LiveData<Boolean> get() = mutableDirectoryCreationMode
    val notebookCreationMode: LiveData<Boolean> get() = mutableNotebookCreationMode

    val contextMenuActive: LiveData<Boolean> get() = mutableContextMenuActive
    val contextDirectories: LiveData<List<Directory>> get() = mutableContextDirectories
    val contextNotebooks: LiveData<List<Notebook>> get() = mutableContextNotebooks
    val contextActions: LiveData<List<ContextAction>> get() = mutableContextActions
    val contextActionRename: LiveData<Boolean> get() = mutableContextActionRename
    val contextActionPaste: LiveData<Boolean> get() = mutableContextActionPaste

    val navigationTitle: LiveData<String> get() = mutableNavigationTitle

    val askingConfirmation: LiveData<Boolean> get() = mutableAskingConfirmation

    private lateinit var fileTree: FileTree

    private val mutableDirectories = MutableLiveData<List<Directory>>()
    private val mutableNotebooks = MutableLiveData<List<Notebook>>()
    private val mutableAddMenuActive = MutableLiveData(false)

    private val mutableDirectoryCreationMode = MutableLiveData(false)
    private val mutableNotebookCreationMode = MutableLiveData(false)

    private val mutableContextMenuActive = MutableLiveData(false)
    private val mutableContextDirectories = MutableLiveData<List<Directory>>(listOf())
    private val mutableContextNotebooks = MutableLiveData<List<Notebook>>(listOf())
    private val mutableContextActions = MutableLiveData<List<ContextAction>>(listOf())
    private val mutableContextActionRename = MutableLiveData<Boolean>(false)
    private val mutableContextActionPaste = MutableLiveData<Boolean>(false)

    private val mutableNavigationTitle = MutableLiveData<String>("My Notebooks")

    private val mutableAskingConfirmation = MutableLiveData<Boolean>(false)

    enum class ContextAction {
        MOVE, RENAME, DELETE, MERGE, PASTE
    }

    fun loadFileTree(rootUUID: UUID) {
        GlobalScope.launch(Dispatchers.Main) {
            fileTree = FileTreeRepository.fileTree(rootUUID)

            updateLevel()
        }
    }

    fun moveDown(directory: Directory): Boolean {
        if (fileTree.moveDown(directory)) {
            updateLevel()
            mutableNavigationTitle.value = directory.name
            return true
        }

        return false
    }

    fun moveUp(): Boolean {
        if (fileTree.moveUp()) {
            updateLevel()

            if (fileTree.current == fileTree.root)
                mutableNavigationTitle.value = "My Notebooks"
            else
                mutableNavigationTitle.value = fileTree.current.directory!!.name

            return true
        }

        return false
    }

    fun toggleNotebookVisibility(notebook: Notebook) {
        GlobalScope.launch(Dispatchers.Main) {
            FileTreeRepository.toggleNotebookVisibility(notebook)
            mutableNotebooks.value = null

            updateLevel()
        }
    }

    fun createDirectoryMode() {
        toggleAddMenu()
        mutableDirectoryCreationMode.value = true
    }

    fun createNotebookMode() {
        toggleAddMenu()
        mutableNotebookCreationMode.value = true
    }

    fun finishDialog(name: String?, userUUID: UUID) {
        if (mutableContextActionRename.value!!) {
            if (name == null) {
                exitContextMenu()
            } else if (name.isNotBlank()) {
                GlobalScope.launch(Dispatchers.Main) {

                    if (mutableContextDirectories.value!!.isNotEmpty())
                        FileTreeRepository.renameDirectory(
                            name,
                            mutableContextDirectories.value!!.first()
                        )
                    else
                        FileTreeRepository.renameNotebook(
                            name,
                            mutableContextNotebooks.value!!.first()
                        )

                    exitContextMenu()
                    updateLevel()
                }
            }

            return
        }

        if (mutableDirectoryCreationMode.value!!) {
            if (name == null) {
                mutableDirectoryCreationMode.value = false
            } else if (name.isNotBlank()) {
                mutableDirectoryCreationMode.value = false

                GlobalScope.launch(Dispatchers.Main) {
                    FileTreeRepository.createDirectory(name, fileTree.current, userUUID)
                    updateLevel()
                }
            }
        } else {
            if (name == null) {
                mutableNotebookCreationMode.value = false
            } else if (name.isNotBlank()) {
                mutableNotebookCreationMode.value = false

                GlobalScope.launch(Dispatchers.Main) {
                    FileTreeRepository.createNotebook(name, fileTree.current, userUUID)
                    updateLevel()
                }
            }
        }
    }

    fun toggleAddMenu() {
        mutableAddMenuActive.value = !mutableAddMenuActive.value!!
    }

    fun longNodeClick(directory: Directory?, notebook: Notebook?) {
        if (mutableContextMenuActive.value!!) return

        mutableContextActions.value = listOf()
        mutableContextMenuActive.value = true

        contextNodeClick(directory, notebook)
    }

    fun contextNodeClick(directory: Directory?, notebook: Notebook?) {
        if (directory != null) {
            val newDirectories = mutableContextDirectories.value!!.toMutableList()

            var found = false
            for (dir in newDirectories) {
                if (dir == directory) {
                    found = true
                    break
                }
            }

            if (!found) {
                newDirectories.add(directory)
            } else {
                newDirectories.remove(directory)
            }

            mutableContextDirectories.value = newDirectories
        } else {
            val newNotebooks = mutableContextNotebooks.value!!.toMutableList()

            var found = false
            for (dir in newNotebooks) {
                if (dir == notebook) {
                    found = true
                    break
                }
            }

            if (!found) {
                newNotebooks.add(notebook!!)
            } else {
                newNotebooks.remove(notebook!!)
            }

            mutableContextNotebooks.value = newNotebooks
        }

        if (mutableContextDirectories.value!!.isEmpty() && mutableContextNotebooks.value!!.isEmpty()) {
            exitContextMenu()
        } else {
            val contextActions = mutableListOf<ContextAction>()
            contextActions.add(ContextAction.DELETE)

            if (mutableContextDirectories.value!!.isEmpty())
                contextActions.add(ContextAction.MOVE)

            if (mutableContextNotebooks.value!!.size + mutableContextDirectories.value!!.size == 1) {
                contextActions.add(ContextAction.RENAME)
            } else if (mutableContextNotebooks.value!!.size > 1 && mutableContextDirectories.value!!.isEmpty()) {
                contextActions.add(ContextAction.MERGE)
            }

            mutableContextActions.value = contextActions
        }
    }

    private fun contextDeleteNodes() {
        GlobalScope.launch(Dispatchers.Main) {
            for (dir in mutableContextDirectories.value!!)
                FileTreeRepository.deleteDirectory(fileTree.current, dir)
            for (note in mutableContextNotebooks.value!!)
                FileTreeRepository.deleteNotebook(fileTree.current, note)

            updateLevel()
            exitContextMenu()
        }
    }

    fun contextPaste() {
        GlobalScope.launch(Dispatchers.Main) {

            for (note in mutableContextNotebooks.value!!)
                FileTreeRepository.moveNotebook(fileTree, fileTree.current, note)

            updateLevel()
            exitContextMenu()
        }
    }

    fun contextRenameMode() {
        mutableContextActionRename.value = true
    }

    fun contextPasteMode() {
        mutableContextActionPaste.value = true

        mutableContextActions.value = listOf(ContextAction.PASTE)
    }

    fun exitContextMenu() {
        mutableContextDirectories.value = mutableListOf()
        mutableContextNotebooks.value = mutableListOf()

        mutableContextActionRename.value = false
        mutableContextActionPaste.value = false
        mutableContextMenuActive.value = false
    }

    private fun updateLevel() {
        val dirs: MutableList<Directory> = mutableListOf()
        val notes: MutableList<Notebook> = mutableListOf()

        for (directory in fileTree.current.directories) {
            dirs.add(directory.directory!!)
        }
        for (notebook in fileTree.current.notebooks) {
            notes.add(notebook.notebook!!)
        }

        dirs.sortBy { it.name }
        notes.sortBy { it.name }

        mutableDirectories.value = dirs
        mutableNotebooks.value = notes
    }

    fun askConfirmation() {
        mutableAskingConfirmation.value = true
    }

    fun acceptConfirmation() {
        contextDeleteNodes()
        mutableAskingConfirmation.value = false
    }

    fun declineConfirmation() {
        mutableAskingConfirmation.value = false
    }

    fun mergeNotebooks(userUUID: UUID) {
        GlobalScope.launch(Dispatchers.Main) {
            val notebookUUIDs = mutableListOf<UUID>()
            for (notebook in mutableContextNotebooks.value!!) {
                notebookUUIDs.add(notebook.id.value)
            }

            NotebookRepository.merge(
                notebookUUIDs,
                userUUID,
                fileTree.current.directory!!.id.value,
                fileTree.current,
                "token"
            )

            updateLevel()
            exitContextMenu()
        }
    }
}