package fer.meisters.diginote.fragments

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import fer.meisters.diginote.databinding.FragmentRegisterUsernameBinding

class RegisterUsername : Fragment(), TextWatcher {
    private var callback: OnUsernameChangedListener? = null

    private var _binding: FragmentRegisterUsernameBinding? = null
    private val binding get() = _binding!!
    
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentRegisterUsernameBinding.inflate(inflater, container, false)

        binding.registerUsername.editText?.addTextChangedListener(this)

        return binding.root
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        callback = context as? OnUsernameChangedListener
        if (callback == null) {
            throw ClassCastException("$context must implement OnArticleSelectedListener")
        }
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
    override fun afterTextChanged(s: Editable?) {
        callback?.onUsernameChanged(binding.registerUsername.editText?.text.toString(), binding)
    }

    interface OnUsernameChangedListener {
        fun onUsernameChanged(username: String, binding: FragmentRegisterUsernameBinding)
    }
}  