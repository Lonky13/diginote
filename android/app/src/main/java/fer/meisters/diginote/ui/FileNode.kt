package fer.meisters.diginote.ui

import android.content.Intent
import android.util.Log
import androidx.compose.foundation.ScrollableColumn
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.horizontalScroll
import androidx.compose.foundation.layout.*
import androidx.compose.runtime.getValue
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.material.ripple.rememberRipple
import androidx.compose.runtime.Composable
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.platform.AmbientContext
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.viewModel
import fer.meisters.diginote.NotebookActivity
import fer.meisters.diginote.database.models.Directory
import fer.meisters.diginote.database.models.Notebook
import fer.meisters.diginote.viewmodels.HomeViewModel


@Composable
fun FileNode(viewModel: HomeViewModel = viewModel()) {
    val directories by viewModel.directories.observeAsState(initial = null)
    val notebooks by viewModel.notebooks.observeAsState(initial = null)

    val contextDirectories by viewModel.contextDirectories.observeAsState(initial = mutableListOf())
    val contextNotebooks by viewModel.contextNotebooks.observeAsState(initial = mutableListOf())

    if (directories == null || notebooks == null) {
        Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
//            Text(text = "Loading...", color = Color.Gray)
            CircularProgressIndicator()
        }
        return
    } else if (directories!!.isEmpty() and notebooks!!.isEmpty()) {
        Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
            Text(text = "Nothing here yet.", color = Color.Gray)
        }
    }

    ScrollableColumn(
        modifier = Modifier.fillMaxSize().padding(10.dp),
        verticalArrangement = Arrangement.spacedBy(10.dp),
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        for (directory in directories!!) {
            DirectoryCard(directory, contextDirectories.contains(directory))
        }
        for (notebook in notebooks!!) {
            NotebookCard(notebook, contextNotebooks.contains(notebook))
        }
    }
}

@Composable
fun NodeCard(
    directory: Directory? = null,
    notebook: Notebook? = null,
    contextual: Boolean = false,
    viewModel: HomeViewModel = viewModel()
) {
    val context = AmbientContext.current

    val contextMenuActive by viewModel.contextMenuActive.observeAsState(initial = false)
    val contextActionPaste by viewModel.contextActionPaste.observeAsState(initial = false)

    val notebooks by viewModel.notebooks.observeAsState(initial = null)

    Card(
        shape = RoundedCornerShape(4.dp),
        backgroundColor = if (contextual) MaterialTheme.colors.surface else if (directory == null) MaterialTheme.colors.secondaryVariant else MaterialTheme.colors.secondary,
        modifier = Modifier.fillMaxWidth().height(100.dp)
    ) {
        Row(
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically
        ) {
            Row(
                horizontalArrangement = Arrangement.Start,
                verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier.weight(0.8f).clickable(
                    onClick = {
                        if (contextMenuActive) {
                            if (contextActionPaste) {
                                if (directory != null)
                                    viewModel.moveDown(directory)
                            } else {
                                viewModel.contextNodeClick(directory, notebook)
                            }
                        } else {
                            if (directory != null) {
                                viewModel.moveDown(directory)
                            } else {
                                val intent = Intent(context, NotebookActivity::class.java)
                                intent.putExtra("notebookUUID", notebook!!.id.value.toString())
                                context.startActivity(intent)
                            }
                        }
                    },
                    onLongClick = {
                        viewModel.longNodeClick(directory, notebook)
                    },
                    indication = rememberRipple(bounded = true)
                )
            ) {
                Icon(
                    imageVector = if (directory == null) Icons.Filled.MenuBook else Icons.Filled.Folder,
                    tint = MaterialTheme.colors.onSecondary,
                    modifier = Modifier.fillMaxHeight().padding(20.dp)
                )
                Text(
                    text = if (directory == null) notebook!!.name else directory.name!!,
                    color = MaterialTheme.colors.onSecondary
                )
            }
            if (directory == null) {
                Icon(
                    imageVector = if (notebook!!.public) Icons.Filled.Public else Icons.Filled.Lock,
                    tint = MaterialTheme.colors.onSecondary,
                    modifier = Modifier.fillMaxHeight().weight(0.2f)
                        .clickable(
                            onClick = { viewModel.toggleNotebookVisibility(notebook) },
                            indication = rememberRipple (bounded = true)
                        )
                )
            }
        }
    }
}

@Composable
fun DirectoryCard(
    directory: Directory,
    contextual: Boolean = false,
    viewModel: HomeViewModel = viewModel()
) {
    NodeCard(directory = directory, contextual = contextual)
}

@Composable
fun NotebookCard(
    notebook: Notebook,
    contextual: Boolean = false,
    viewModel: HomeViewModel = viewModel()
) {
    NodeCard(notebook = notebook, contextual = contextual)
}