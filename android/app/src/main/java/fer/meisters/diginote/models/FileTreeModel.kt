package fer.meisters.diginote.models

import fer.meisters.diginote.database.models.Directory
import fer.meisters.diginote.database.models.Notebook

class FileTree(var current: Node, val root: Node = current) {
    data class Node(
        val parent: Node?,
        val directories: MutableList<Node>,
        val notebooks: MutableList<Node>,
        val directory: Directory?,
        val notebook: Notebook?
    )

    fun moveUp(): Boolean {
        current.parent?.apply {
            current = current.parent!!
            return true
        }
        return false
    }

    fun moveDown(directory: Directory): Boolean {
        for (child in current.directories) {
            if (child.directory != null && child.directory == directory) {
                current = child
                return true
            }
        }
        return false
    }
}
