package fer.meisters.diginote

import android.app.Activity
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.focusable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.savedinstancestate.savedInstanceState
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.platform.AmbientContext
import androidx.compose.ui.platform.setContent
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.viewModel
import androidx.core.app.ActivityCompat.finishAffinity
import androidx.core.content.ContextCompat.startActivity
import fer.meisters.diginote.models.UserError
import fer.meisters.diginote.ui.AppTheme
import fer.meisters.diginote.ui.ConfirmationDialog
import fer.meisters.diginote.ui.ContextualButton
import fer.meisters.diginote.ui.NavigationBar
import fer.meisters.diginote.viewmodels.ProfileViewModel
import io.jsonwebtoken.Jwts
import java.util.*

class ProfileActivity : AppCompatActivity() {

    companion object {
        const val TAG = "ProfileActivity"
    }

    private val viewModel: ProfileViewModel by viewModels()

    private lateinit var username: String
    private lateinit var email: String
    private lateinit var userUUID: UUID
    private lateinit var rootUUID: UUID

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.i(TAG, "Activity created.")

        // Extract user info from jwt token
        val userPreferences: SharedPreferences = getSharedPreferences("user", MODE_PRIVATE)
        val userInfo = Jwts.parser().setSigningKey(BuildConfig.JWT_SECRET.toByteArray())
            .parseClaimsJws(
                userPreferences.getString("jwt_token", "This user does not exist!")
            ).body

        username = userInfo["username"] as String
        email = userInfo["email"] as String
        userUUID = UUID.fromString(userInfo["uuid"] as String)
        rootUUID = UUID.fromString(userInfo["rootUUID"] as String)

        viewModel.setInfo(username, email)

        setContent {
            val context = AmbientContext.current

            val askingConfirmation by viewModel.askingConfirmation.observeAsState(initial = false)

            AppTheme {

                if (askingConfirmation) {
                    ConfirmationDialog(
                        prompt = "Are you sure you want to permanently delete your account?",
                        onConfirm = {
                            viewModel.deleteProfile(userUUID)
                            val user: SharedPreferences =
                                context.getSharedPreferences("user", AppCompatActivity.MODE_PRIVATE)
                            val editUser: SharedPreferences.Editor = user.edit()
                            editUser.clear().apply()

                            context.startActivity(Intent(context, LauncherActivity::class.java))
                            finishAffinity(context as Activity)
                            viewModel.acceptConfirmation(userUUID)
                        },
                        onDecline = { viewModel.declineConfirmation() })
                }

                Scaffold(topBar = {
                    EditContextualBar(userUUID)
                }, floatingActionButton = {
                    ExtendedFloatingActionButton(
                        text = {
                            Text(text = "Logout")
                        },
                        onClick = {
                            val user: SharedPreferences =
                                getSharedPreferences("user", MODE_PRIVATE)
                            val editUser: SharedPreferences.Editor = user.edit()
                            editUser.clear().apply()

                            startActivity(Intent(context, LauncherActivity::class.java))
                            finishAffinity()

                        },
                        icon = {
                            Icon(
                                imageVector = Icons.Filled.ExitToApp,
                                tint = Color.White
                            )
                        }
                    )
                },
                    bodyContent = {
                        Column(
                            verticalArrangement = Arrangement.spacedBy(10.dp),
                            modifier = Modifier.fillMaxSize().padding(10.dp)
                        ) {
                            val editMode by viewModel.editMode.observeAsState(initial = false)

                            if (editMode) {
                                val errors by viewModel.errors.observeAsState(initial = mutableListOf())
                                // These field are gonna be disabled once that features becomes available
                                ProfileTextField(
                                    "Username",
                                    viewModel.username,
                                    Icons.Filled.AccountCircle,
                                    if (errors.contains(UserError.USERNAME_MAX_LENGTH)) {
                                        getString(R.string.username_max_length)
                                    } else if (errors.contains(UserError.USERNAME_MIN_LENGTH)) {
                                        getString(R.string.username_min_length)
                                    } else if (errors.contains(UserError.USERNAME_NO_SPACES)) {
                                        getString(R.string.username_no_spaces)
                                    } else if (errors.contains(UserError.USERNAME_CAN_NOT_BE_EMAIL)) {
                                        getString(R.string.username_can_not_be_email)
                                    } else if (errors.contains(UserError.USERNAME_EXISTS)) {
                                        getString(R.string.username_exists)
                                    } else null,
                                    { viewModel.newUsername = it })
                                ProfileTextField(
                                    "Email",
                                    viewModel.email,
                                    Icons.Filled.Email,
                                    if (errors.contains(UserError.EMAIL_MAX_LENGTH)) {
                                        getString(R.string.email_max_length)
                                    } else if (errors.contains(UserError.EMAIL_EXISTS)) {
                                        getString(R.string.email_exists)
                                    } else if (errors.contains(UserError.INVALID_EMAIL)) {
                                        getString(R.string.invalid_email)
                                    } else null,
                                    { viewModel.newEmail = it })

                                ProfileTextField(
                                    "New password",
                                    "",
                                    Icons.Filled.VpnKey,
                                    if (errors.contains(UserError.PASSWORD_NO_SPACES)) {
                                        getString(R.string.password_no_spaces)
                                    } else if (errors.contains(UserError.PASSWORD_MIN_LENGTH)) {
                                        getString(R.string.password_min_length)
                                    } else null,
                                    { viewModel.newPassword = it },
                                    true
                                )
                                ProfileTextField(
                                    "Confirm password",
                                    "",
                                    Icons.Filled.Check,
                                    if (errors.contains(UserError.PASSWORDS_DO_NOT_MATCH)) {
                                        getString(R.string.passwords_do_not_match)
                                    } else null,
                                    { viewModel.confirmPassword = it },
                                    true
                                )
                            } else {
                                ProfileTextField(
                                    "Username",
                                    viewModel.username,
                                    Icons.Filled.AccountCircle
                                )
                                ProfileTextField("Email", viewModel.email, Icons.Filled.Email)
                            }
                        }
                    })
            }
        }
    }

    override fun onBackPressed() {
        if (!viewModel.editMode.value!!)
            super.onBackPressed()
        else
            viewModel.exitEditMode()
    }
}

@Composable
private fun ProfileTextField(
    label: String,
    value: String,
    icon: ImageVector,
    error: String? = null,
    onValueChange: (String) -> Unit = {},
    hidden: Boolean = false,
    viewModel: ProfileViewModel = viewModel()
) {
    val editMode by viewModel.editMode.observeAsState(initial = false)

    var text by savedInstanceState { value }
    TextField(
        modifier = Modifier.fillMaxWidth().height(65.dp),
        visualTransformation = if (!hidden) VisualTransformation.None else PasswordVisualTransformation(),
        backgroundColor = if (editMode) MaterialTheme.colors.surface else MaterialTheme.colors.secondary,
        value = text,
        isErrorValue = error != null,
        singleLine = true,
        activeColor = MaterialTheme.colors.onSurface,
        onValueChange = {
            text = if (it.length <= 25) it else text

            onValueChange(text)
        },
        leadingIcon = {
            Icon(
                imageVector = icon,
                tint = MaterialTheme.colors.onSurface
            )
        },
        trailingIcon = {
            if (error != null)
                Icon(
                    imageVector = Icons.Filled.Error,
                )
        },
//        label = { Text(label + if (error != null) " - $error" else "") }
        label = { Text(error ?: label) }
    )
}

@Composable
private fun EditContextualBar(userUUID: UUID, viewModel: ProfileViewModel = viewModel()) {
    val context = AmbientContext.current

    val editMode by viewModel.editMode.observeAsState(initial = false)

    if (editMode) {
        Row(
            modifier = Modifier.fillMaxWidth().height(65.dp)
                .background(MaterialTheme.colors.secondary),
            horizontalArrangement = Arrangement.SpaceEvenly,

            ) {

            ContextualButton(
                icon = Icons.Filled.DeleteForever,
                onClick = {
                    viewModel.askConfirmation()
                })

            ContextualButton(
                icon = Icons.Filled.Save,
                onClick = { viewModel.saveChanges(userUUID) })

            ContextualButton(
                icon = Icons.Filled.Cancel,
                onClick = { viewModel.exitEditMode() })
        }
    } else {
        NavigationBar(
            "My Profile",
            Icons.Filled.Home,
            Icons.Filled.Edit,
            {
                context.startActivity(
                    Intent(
                        context,
                        HomeActivity::class.java
                    )
                )

            },
            {
                viewModel.enterEditMode()
            }
        )
    }
}
