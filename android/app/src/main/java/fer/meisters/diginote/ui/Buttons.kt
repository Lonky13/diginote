package fer.meisters.diginote.ui

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.width
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.ripple.rememberRipple
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.unit.dp

@Composable
fun ContextualButton(icon: ImageVector, onClick: () -> Unit) {
    Box(
        modifier = Modifier.fillMaxHeight().width(65.dp).clickable(
            onClick = onClick,
            indication = rememberRipple(bounded = true)
        )
    ) {
        Icon(
            imageVector = icon,
            tint = MaterialTheme.colors.onPrimary,
            modifier = Modifier.fillMaxHeight().fillMaxWidth()
        )
    }
}