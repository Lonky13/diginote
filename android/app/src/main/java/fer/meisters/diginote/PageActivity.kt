package fer.meisters.diginote

import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.ScrollState
import androidx.compose.foundation.ScrollableColumn
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.CoreTextField
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.material.Icon
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Code
import androidx.compose.material.icons.filled.InsertPhoto
import androidx.compose.material.icons.filled.SettingsOverscan
import androidx.compose.material.icons.filled.Title
import androidx.compose.material.icons.filled.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.savedinstancestate.savedInstanceState
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.setContent
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.viewModel
import fer.meisters.diginote.ui.AppTheme
import fer.meisters.diginote.ui.PageCanvas
import fer.meisters.diginote.viewmodels.PageViewModel
import java.io.File
import java.util.*

class PageActivity : AppCompatActivity() {

    companion object {
        const val TAG = "PageActivity"
    }

    private lateinit var notebookUUID: UUID
    private var position: Int = -1

    private val viewModel: PageViewModel by viewModels()

    private lateinit var file: File

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Log.i(TAG, "Activity created.")

        notebookUUID = UUID.fromString(intent.getStringExtra("notebookUUID"))
        position = intent.getIntExtra("position", -1)

        file = File(this.cacheDir, notebookUUID.toString())
        viewModel.setFile(file, position, notebookUUID)

        setContent {
            AppTheme {
                val loading by viewModel.loading.observeAsState(initial = false)
                val OCRResults by viewModel.OCRResults.observeAsState(initial = null)
                val codeMode by viewModel.codeMode.observeAsState(false)
                val code by viewModel.code.observeAsState("")

                if (loading) {
                    Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
                        CircularProgressIndicator()
                    }
                    return@AppTheme
                }

                if (OCRResults != null) {
                    Scaffold(
                        floatingActionButton = {
                            FloatingActionButton(onClick = { viewModel.exitOCRResults() }) {
                                Icon(
                                    imageVector = Icons.Filled.Cancel,
                                    tint = Color.White,
                                )
                            }
                        },
                        bodyContent = {
                            ScrollableColumn(
                                modifier = Modifier
                                    .background(Color.White)
                                    .fillMaxSize(),
                                horizontalAlignment = Alignment.CenterHorizontally,
                                verticalArrangement = Arrangement.Center
                            ) {
                                Text(
                                    text = OCRResults!!,
                                    color = Color.Black,
                                )
                            }
                        }
                    )
                    return@AppTheme
                }

                if (codeMode) {
                    var text by savedInstanceState { code }

                    Scaffold(
                        floatingActionButton = {
                            Column(
                                modifier = Modifier.padding(10.dp),
                                verticalArrangement = Arrangement.spacedBy(10.dp)
                            ) {
                                FloatingActionButton(onClick = { viewModel.runCode() }) {
                                    Icon(
                                        imageVector = Icons.Filled.PlayArrow,
                                        tint = Color.White,
                                    )
                                }
                                FloatingActionButton(onClick = { viewModel.exitCodeMode() }) {
                                    Icon(
                                        imageVector = Icons.Filled.Cancel,
                                        tint = Color.White,
                                    )
                                }
                            }
                        },
                        bodyContent = {
                            OutlinedTextField(
                                value = text,
                                modifier = Modifier.fillMaxSize(),
                                onValueChange = {
                                    text = it

                                    viewModel.updateCode(text)
                                }
                            )
                        }
                    )
                    return@AppTheme
                }

                Scaffold(
                    bottomBar = {
                        Row(
                            modifier = Modifier.fillMaxSize(),
                            horizontalArrangement = Arrangement.SpaceBetween,
                            verticalAlignment = Alignment.Bottom
                        ) {
                            Column(
                                verticalArrangement = Arrangement.spacedBy(10.dp),
                                modifier = Modifier
                                    .padding(10.dp)
                                    .fillMaxHeight(),
                            ) {
                                FloatingActionButton(
                                    onClick = { viewModel.undo() },
                                    modifier = Modifier.size(30.dp)
                                ) {
                                    Icon(
                                        imageVector = Icons.Filled.Undo,
                                        tint = Color.White,
                                    )
                                }
                                FloatingActionButton(
                                    onClick = {
                                        viewModel.mode = PageViewModel.Mode.MOVE
                                    }, modifier = Modifier.size(30.dp)
                                ) {
                                    Icon(
                                        imageVector = Icons.Filled.SettingsOverscan,
                                        tint = Color.White,
                                    )
                                }
                                FloatingActionButton(onClick = {
                                    viewModel.enterCodeMode()
                                }, modifier = Modifier.size(30.dp)) {
                                    Icon(imageVector = Icons.Filled.Code, tint = Color.White)
                                }
                                FloatingActionButton(onClick = {
                                    viewModel.mode = PageViewModel.Mode.DRAW
                                }, modifier = Modifier.size(30.dp)) {
                                    Icon(imageVector = Icons.Filled.InsertPhoto, tint = Color.White)
                                }
                                FloatingActionButton(
                                    onClick = {
                                        viewModel.mode = PageViewModel.Mode.TEXT
                                    }, modifier = Modifier.size(30.dp)
                                ) {
                                    Icon(imageVector = Icons.Filled.Title, tint = Color.White)
                                }
                            }
                            Column(
                                verticalArrangement = Arrangement.spacedBy(10.dp),
                                horizontalAlignment = Alignment.End,
                                modifier = Modifier
                                    .fillMaxHeight()
                                    .padding(10.dp)
                            ) {
                                StrokeColorFAB()
                                StrokeSizeFAB()
                                ColorFAB()
                                GridFAB()
                                FloatingActionButton(
                                    onClick = { viewModel.OCR() }, modifier = Modifier.size(30.dp)
                                ) {
                                    Icon(imageVector = Icons.Filled.Visibility, tint = Color.White)
                                }
                            }
                        }

                    },
                    bodyContent = {
                        PageCanvas()
                    },
                )
            }
        }
    }

    override fun onPause() {
        super.onPause()

        viewModel.updateContent()
    }

    override fun onBackPressed() {
        if (viewModel.OCRResults.value != null) {
            viewModel.exitOCRResults()
        } else if (viewModel.codeMode.value == true) {
            viewModel.exitCodeMode()
        } else {
            super.onBackPressed()
        }
    }
}

@Composable
private fun ColorFAB(viewModel: PageViewModel = viewModel()) {
    val addColorActive by viewModel.addColorActive.observeAsState(initial = false)

    if (addColorActive) {
        Row(
            horizontalArrangement = Arrangement.spacedBy(5.dp),
        ) {
            FloatingActionButton(
                backgroundColor = MaterialTheme.colors.secondaryVariant,
                modifier = Modifier.size(30.dp),
                onClick = { viewModel.setPageColor(fer.meisters.diginote.models.Color.LightGray) },
            ) {
                Icon(imageVector = Icons.Filled.Stop, tint = Color.LightGray)
            }
            FloatingActionButton(
                backgroundColor = MaterialTheme.colors.secondaryVariant,
                modifier = Modifier.size(30.dp),
                onClick = { viewModel.setPageColor(fer.meisters.diginote.models.Color.DarkGray) },
            ) {
                Icon(imageVector = Icons.Filled.Stop, tint = Color.DarkGray)
            }
            FloatingActionButton(
                backgroundColor = MaterialTheme.colors.secondaryVariant,
                modifier = Modifier.size(30.dp),
                onClick = { viewModel.setPageColor(fer.meisters.diginote.models.Color.Red) },
            ) {
                Icon(imageVector = Icons.Filled.Stop, tint = Color.Red)
            }
            FloatingActionButton(
                backgroundColor = MaterialTheme.colors.secondaryVariant,
                modifier = Modifier.size(30.dp),
                onClick = { viewModel.setPageColor(fer.meisters.diginote.models.Color.White) },
            ) {
                Icon(imageVector = Icons.Filled.Stop, tint = Color.White)
            }
            FloatingActionButton(
                backgroundColor = MaterialTheme.colors.secondaryVariant,
                modifier = Modifier.size(30.dp),
                onClick = { viewModel.setPageColor(fer.meisters.diginote.models.Color.Black) },
            ) {
                Icon(imageVector = Icons.Filled.Stop, tint = Color.Black)
            }
            FloatingActionButton(
                onClick = { viewModel.toggleAddColor() }, modifier = Modifier.size(30.dp)
            ) {
                Icon(imageVector = Icons.Filled.Close, tint = Color.White)
            }
        }
    } else {
        FloatingActionButton(
            onClick = { viewModel.toggleAddColor() }, modifier = Modifier.size(30.dp)
        ) {
            Icon(imageVector = Icons.Filled.Palette, tint = Color.White)
        }
    }
}

@Composable
fun GridFAB(viewModel: PageViewModel = viewModel()) {
    val addGridActive by viewModel.toggleGrid.observeAsState(initial = false)

    if (addGridActive) {
        Row(
            horizontalArrangement = Arrangement.spacedBy(5.dp),
        ) {
            FloatingActionButton(
                backgroundColor = MaterialTheme.colors.secondaryVariant,
                modifier = Modifier.size(30.dp),
                onClick = { viewModel.setGridType(PageViewModel.GridType.DOTS) },
            ) {
                Icon(imageVector = Icons.Filled.BorderClear, tint = Color.White)
            }
            FloatingActionButton(
                backgroundColor = MaterialTheme.colors.secondaryVariant,
                modifier = Modifier.size(30.dp),
                onClick = { viewModel.setGridType(PageViewModel.GridType.SQUARES) },
            ) {
                Icon(imageVector = Icons.Filled.BorderAll, tint = Color.White)
            }
            FloatingActionButton(
                backgroundColor = MaterialTheme.colors.secondaryVariant,
                modifier = Modifier.size(30.dp),
                onClick = { viewModel.setGridType(PageViewModel.GridType.LINES) },
            ) {
                Icon(imageVector = Icons.Filled.FormatAlignJustify, tint = Color.White)
            }
            FloatingActionButton(
                backgroundColor = MaterialTheme.colors.secondaryVariant,
                modifier = Modifier.size(30.dp),
                onClick = { viewModel.setGridType(PageViewModel.GridType.BLANK) },
            ) {
                Icon(imageVector = Icons.Filled.CropSquare, tint = Color.White)
            }
            FloatingActionButton(
                onClick = { viewModel.toggleGrid() }, modifier = Modifier.size(30.dp)
            ) {
                Icon(imageVector = Icons.Filled.Close, tint = Color.White)
            }
        }

    } else {
        FloatingActionButton(
            onClick = { viewModel.toggleGrid() }, modifier = Modifier.size(30.dp)
        ) {
            Icon(imageVector = Icons.Filled.Dehaze, tint = Color.White)
        }
    }
}

@Composable
private fun StrokeColorFAB(viewModel: PageViewModel = viewModel()) {
    val strokeColorActive by viewModel.strokeColorActive.observeAsState(initial = false)

    if (strokeColorActive) {
        Row(
            horizontalArrangement = Arrangement.spacedBy(5.dp),
        ) {
            FloatingActionButton(
                backgroundColor = MaterialTheme.colors.secondaryVariant,
                modifier = Modifier.size(30.dp),
                onClick = { viewModel.setStrokeColor(fer.meisters.diginote.models.Color.LightGray) },
            ) {
                Icon(imageVector = Icons.Filled.Stop, tint = Color.LightGray)
            }
            FloatingActionButton(
                backgroundColor = MaterialTheme.colors.secondaryVariant,
                modifier = Modifier.size(30.dp),
                onClick = { viewModel.setStrokeColor(fer.meisters.diginote.models.Color.DarkGray) },
            ) {
                Icon(imageVector = Icons.Filled.Stop, tint = Color.DarkGray)
            }
            FloatingActionButton(
                backgroundColor = MaterialTheme.colors.secondaryVariant,
                modifier = Modifier.size(30.dp),
                onClick = { viewModel.setStrokeColor(fer.meisters.diginote.models.Color.Red) },
            ) {
                Icon(imageVector = Icons.Filled.Stop, tint = Color.Red)
            }
            FloatingActionButton(
                backgroundColor = MaterialTheme.colors.secondaryVariant,
                modifier = Modifier.size(30.dp),
                onClick = { viewModel.setStrokeColor(fer.meisters.diginote.models.Color.White) },
            ) {
                Icon(imageVector = Icons.Filled.Stop, tint = Color.White)
            }
            FloatingActionButton(
                backgroundColor = MaterialTheme.colors.secondaryVariant,
                modifier = Modifier.size(30.dp),
                onClick = { viewModel.setStrokeColor(fer.meisters.diginote.models.Color.Blue) },
            ) {
                Icon(imageVector = Icons.Filled.Stop, tint = Color.Blue)
            }
            FloatingActionButton(
                backgroundColor = MaterialTheme.colors.secondaryVariant,
                modifier = Modifier.size(30.dp),
                onClick = { viewModel.setStrokeColor(fer.meisters.diginote.models.Color.Green) },
            ) {
                Icon(imageVector = Icons.Filled.Stop, tint = Color.Green)
            }
            FloatingActionButton(
                backgroundColor = MaterialTheme.colors.secondaryVariant,
                modifier = Modifier.size(30.dp),
                onClick = { viewModel.setStrokeColor(fer.meisters.diginote.models.Color.Black) },
            ) {
                Icon(imageVector = Icons.Filled.Stop, tint = Color.Black)
            }
            FloatingActionButton(
                onClick = { viewModel.toggleStrokeColor() }, modifier = Modifier.size(30.dp)
            ) {
                Icon(imageVector = Icons.Filled.Close, tint = Color.White)
            }
        }
    } else {
        FloatingActionButton(
            onClick = { viewModel.toggleStrokeColor() }, modifier = Modifier.size(30.dp)
        ) {
            Icon(imageVector = Icons.Filled.FormatPaint, tint = Color.White)
        }
    }
}

@Composable
private fun StrokeSizeFAB(viewModel: PageViewModel = viewModel()) {
    val strokeSizeActive by viewModel.strokeSizeActive.observeAsState(initial = false)

    if (strokeSizeActive) {
        Row(
            horizontalArrangement = Arrangement.spacedBy(5.dp),
        ) {
            FloatingActionButton(
                backgroundColor = MaterialTheme.colors.secondaryVariant,
                modifier = Modifier.size(30.dp),
                onClick = { viewModel.setStrokeSize(10f) },
            ) {
                Text(
                    text = "10",
                    color = Color.White
                )
            }
            FloatingActionButton(
                backgroundColor = MaterialTheme.colors.secondaryVariant,
                modifier = Modifier.size(30.dp),
                onClick = { viewModel.setStrokeSize(15f) },
            ) {
                Text(
                    text = "15",
                    color = Color.White
                )
            }
            FloatingActionButton(
                backgroundColor = MaterialTheme.colors.secondaryVariant,
                modifier = Modifier.size(30.dp),
                onClick = { viewModel.setStrokeSize(20f) },
            ) {
                Text(
                    text = "20",
                    color = Color.White
                )
            }
            FloatingActionButton(
                onClick = { viewModel.toggleStrokeSize() }, modifier = Modifier.size(30.dp)
            ) {
                Icon(imageVector = Icons.Filled.Close, tint = Color.White)
            }
        }
    } else {
        FloatingActionButton(
            onClick = { viewModel.toggleStrokeSize() }, modifier = Modifier.size(30.dp)
        ) {
            Icon(imageVector = Icons.Filled.FormatSize, tint = Color.White)
        }
    }
}