package fer.meisters.diginote

import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.savedinstancestate.savedInstanceState
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.AmbientContext
import androidx.compose.ui.platform.setContent
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.viewModel
import androidx.compose.ui.window.Dialog
import fer.meisters.diginote.ui.*
import fer.meisters.diginote.viewmodels.HomeViewModel
import io.jsonwebtoken.Jwts
import java.util.*

class HomeActivity : AppCompatActivity() {

    companion object {
        const val TAG = "HomeActivity"
    }

    private lateinit var userUUID: UUID
    private lateinit var rootUUID: UUID

    private val viewModel: HomeViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.i(TAG, "Activity created.")

        setContent {
            val askingConfirmation by viewModel.askingConfirmation.observeAsState(initial = false)

            AppTheme {
                
                if(askingConfirmation){
                    ConfirmationDialog(
                        prompt = "Are you sure you want to permanently delete selected?",
                        onConfirm = { viewModel.acceptConfirmation() },
                        onDecline = { viewModel.declineConfirmation() })
                }

                Scaffold(topBar = {
                    HomeContextualBar(userUUID)
                }, floatingActionButton = { HomeFAB() },
                    bodyContent = {
                        DialogWrapper(userUUID)
                        FileNode()
                    })
            }
        }

        // Extract user info from jwt token
        val userPreferences: SharedPreferences = getSharedPreferences("user", MODE_PRIVATE)
        val userInfo = Jwts.parser().setSigningKey(BuildConfig.JWT_SECRET.toByteArray())
            .parseClaimsJws(
                userPreferences.getString("jwt_token", "This user does not exist!")
            ).body

        userUUID = UUID.fromString(userInfo["uuid"] as String)
        rootUUID = UUID.fromString(userInfo["rootUUID"] as String)

        viewModel.loadFileTree(rootUUID = rootUUID)
    }

    override fun onBackPressed() {
        if(viewModel.contextMenuActive.value!! && !viewModel.contextActionPaste.value!!){
            viewModel.exitContextMenu()
            return
        }

        if (!viewModel.moveUp())
            super.onBackPressed()
    }
}

@Composable
private fun HomeContextualBar(userUUID: UUID, viewModel: HomeViewModel = viewModel()) {
    val context = AmbientContext.current

    val contextMenuActive by viewModel.contextMenuActive.observeAsState(initial = false)
    val contextActions by viewModel.contextActions.observeAsState(initial = listOf())
    
    val navigationTitle by viewModel.navigationTitle.observeAsState(initial = "My Notebooks")

    if (contextMenuActive) {
        Row(
            modifier = Modifier.fillMaxWidth().height(65.dp)
                .background(MaterialTheme.colors.secondary),
            horizontalArrangement = Arrangement.SpaceEvenly,

            ) {
            if (contextActions.contains(HomeViewModel.ContextAction.PASTE)) {
                ContextualButton(Icons.Filled.ContentPaste) { viewModel.contextPaste() }
            } else {
                if (contextActions.contains(HomeViewModel.ContextAction.MERGE)) {
                    ContextualButton(Icons.Filled.MergeType) { viewModel.mergeNotebooks(userUUID) }
                }

                if (contextActions.contains(HomeViewModel.ContextAction.RENAME)) {
                    ContextualButton(Icons.Filled.Create) { viewModel.contextRenameMode() }
                }

                if (contextActions.contains(HomeViewModel.ContextAction.MOVE)) {
                    ContextualButton(Icons.Filled.ContentCut) { viewModel.contextPasteMode() }
                }

                ContextualButton(
                    icon = Icons.Filled.Delete,
                    onClick = { viewModel.askConfirmation() })
            }
        }
    } else {
        NavigationBar(
            navigationTitle,
            Icons.Filled.Public,
            Icons.Filled.AccountCircle,
            {
                context.startActivity(
                    Intent(
                        context,
                        PublicNotebooksActivity::class.java
                    )
                )

            },
            {
                context.startActivity(
                    Intent(
                        context,
                        ProfileActivity::class.java
                    )
                )
            }
        )
    }
}

@Composable
private fun InputDialog(title: String, acceptText: String, userUUID: UUID, viewModel: HomeViewModel = viewModel()) {
    var text by savedInstanceState { "" }
    Dialog(onDismissRequest = { }) {
        Card {
            Column(horizontalAlignment = Alignment.CenterHorizontally) {
                Text(text = title, modifier = Modifier.padding(20.dp))
                TextField(
                    modifier = Modifier.fillMaxWidth().padding(horizontal = 20.dp),
                    value = text,
                    singleLine = true,
                    activeColor = MaterialTheme.colors.onSurface,
                    onValueChange = { text = if (it.length <= 20) it else text },
                    label = { Text("Name") }
                )
                Row(
                    horizontalArrangement = Arrangement.SpaceEvenly,
                    modifier = Modifier.padding(20.dp).fillMaxWidth()
                ) {
                    TextButton(
                        onClick = { viewModel.finishDialog(text, userUUID) }) {
                        Text(text = acceptText, color = MaterialTheme.colors.onSurface)
                    }
                    TextButton(onClick = { viewModel.finishDialog(null, userUUID) }) {
                        Text(text = "Cancel", color = MaterialTheme.colors.onSurface)
                    }
                }
            }
        }
    }
}

@Composable
private fun DialogWrapper(userUUID: UUID, viewModel: HomeViewModel = viewModel()) {
    val directoryCreationMode by viewModel.directoryCreationMode.observeAsState(initial = false)
    val notebookCreationMode by viewModel.notebookCreationMode.observeAsState(initial = false)
    val contextActionRename by viewModel.contextActionRename.observeAsState(initial = false)

    if (directoryCreationMode) {
        InputDialog("Create a new directory", "Create", userUUID)
    } else if (notebookCreationMode) {
        InputDialog("Create a new notebook", "Create", userUUID)
    } else if(contextActionRename){
        InputDialog("Rename", "Rename", userUUID)
    }
}

@Composable
private fun HomeFAB(viewModel: HomeViewModel = viewModel()) {
    val addMenuActive by viewModel.addMenuActive.observeAsState(initial = false)

    if (addMenuActive) {
        Column(
            verticalArrangement = Arrangement.spacedBy(15.dp),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            FloatingActionButton(
                backgroundColor = MaterialTheme.colors.secondaryVariant,
                modifier = Modifier.size(45.dp),
                onClick = { viewModel.createDirectoryMode() }) {
                Icon(imageVector = Icons.Filled.CreateNewFolder, tint = Color.White)
            }
            FloatingActionButton(
                backgroundColor = MaterialTheme.colors.secondaryVariant,
                modifier = Modifier.size(45.dp),
                onClick = { viewModel.createNotebookMode() }) {
                Icon(imageVector = Icons.Filled.NoteAdd, tint = Color.White)
            }
            FloatingActionButton(
                onClick = { viewModel.toggleAddMenu() }) {
                Icon(imageVector = Icons.Filled.Close, tint = Color.White)
            }
        }
    } else {
        FloatingActionButton(
            onClick = { viewModel.toggleAddMenu() }) {
            Icon(imageVector = Icons.Filled.Add, tint = Color.White)
        }
    }
}