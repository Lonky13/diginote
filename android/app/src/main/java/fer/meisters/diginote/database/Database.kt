package fer.meisters.diginote.database

import fer.meisters.diginote.BuildConfig
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import org.jetbrains.exposed.sql.*

val db_scope = CoroutineScope(Dispatchers.IO)

fun db_connection() : Database {
    return Database.connect(
        "jdbc:postgresql://" + BuildConfig.DIGINOTE_DB_HOST + ":" + BuildConfig.DIGINOTE_DB_PORT + "/" + BuildConfig.DIGINOTE_DB_NAME,
        driver = "org.postgresql.Driver",
        user = BuildConfig.DIGINOTE_DB_USER,
        password = BuildConfig.DIGINOTE_DB_PASSWORD
    )
}