package fer.meisters.diginote.viewmodels

import android.provider.ContactsContract
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.gson.Gson
import fer.meisters.diginote.md5
import fer.meisters.diginote.models.NotebookModel
import fer.meisters.diginote.models.PageModel
import fer.meisters.diginote.repositories.NotebookRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.File
import java.util.*

class NotebookViewModel : ViewModel() {
    val notebook: LiveData<NotebookModel> get() = mutableNotebook
    private val mutableNotebook = MutableLiveData<NotebookModel>(null)

    val selectedPages: LiveData<Set<Int>> get() = mutableSelectedPages
    private val mutableSelectedPages = MutableLiveData<Set<Int>>(setOf())

    val selectionMode: LiveData<Boolean> get() = mutableSelectionMode
    private val mutableSelectionMode = MutableLiveData<Boolean>(false)

    // Cache
    private suspend fun fetchCacheContent(file: File) {
        withContext(Dispatchers.IO) {
            mutableNotebook.postValue(Gson().fromJson(file.readText(), NotebookModel::class.java))
        }
    }

    // Cache
    private suspend fun updateCacheContent(file: File, content: NotebookModel) {
        withContext(Dispatchers.IO) {
            file.writeText(Gson().toJson(content))
        }
    }

    // Server
    fun fetchContent(notebookUUID: UUID, token: String, file: File) {
        GlobalScope.launch(Dispatchers.Main) {
            if (file.exists()) {
                fetchCacheContent(file)
            } else {
                val content = NotebookRepository.fetchContent(notebookUUID, "token")
                mutableNotebook.postValue(content)
                updateCacheContent(file, content)
            }
        }
    }

    // Server
    private fun updateContent(notebookUUID: UUID, content: NotebookModel, token: String) {
        GlobalScope.launch(Dispatchers.Main) {
            NotebookRepository.updateContent(notebookUUID, content, token)
        }
    }

    fun addPage(notebookUUID: UUID, token: String, file: File) {
        GlobalScope.launch(Dispatchers.Main) {
            // Get cached file
            fetchCacheContent(file)

            // Update in memory
            val newNotebook = NotebookModel(notebook.value!!.pages, notebook.value!!.hashes)
            newNotebook.pages.add(PageModel(newNotebook.pages.size))
            newNotebook.hashes.add(
                (newNotebook.pages.last().code + Gson().toJson(
                    newNotebook.pages.last().drawingPath
                ) + Gson().toJson(newNotebook.pages.last().textPath) + Gson().toJson(newNotebook.pages.last().grid) + Gson().toJson(
                    newNotebook.pages.last().color
                )).md5()
            )
            mutableNotebook.postValue(newNotebook)

            // Update cache
            updateCacheContent(file, newNotebook)
            updateContent(notebookUUID, mutableNotebook.value!!, token)
        }
    }

    fun deletePages(notebookUUID: UUID, token: String, file: File) {
        GlobalScope.launch(Dispatchers.Main) {
            mutableSelectionMode.postValue(false)
            fetchCacheContent(file)
            val newNotebook = NotebookModel(mutableListOf(), mutableListOf())

            var position = 0
            for (i in 0 until mutableNotebook.value!!.pages.size) {
                if (!mutableSelectedPages.value!!.contains(mutableNotebook.value!!.pages[i].position)) {
                    newNotebook.pages.add(mutableNotebook.value!!.pages[i])
                    newNotebook.pages.last().position = position
                    position += 1
                    newNotebook.hashes.add(mutableNotebook.value!!.hashes[i])
                }
            }

            mutableSelectedPages.postValue(setOf())
            mutableNotebook.postValue(newNotebook)

            updateCacheContent(file, newNotebook)
            updateContent(notebookUUID, mutableNotebook.value!!, token)
        }
    }

    fun addPageToSelection(position: Int) {
        val pages = mutableSelectedPages.value!!.toMutableSet()
        pages.add(position)
        mutableSelectedPages.value = pages.toSet()
    }

    fun removePageFromSelection(position: Int) {
        val pages = mutableSelectedPages.value!!.toMutableSet()
        pages.remove(position)
        mutableSelectedPages.value = pages.toSet()

        if (pages.size == 0) mutableSelectionMode.value = false
    }

    fun enableSelectionMode() {
        mutableSelectionMode.value = true
    }
}