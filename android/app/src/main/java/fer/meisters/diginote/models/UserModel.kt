package fer.meisters.diginote.models

import fer.meisters.diginote.database.models.User
import fer.meisters.diginote.database.models.Users
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

enum class UserError {
    PASSWORD_MIN_LENGTH, PASSWORD_NO_SPACES,
    PASSWORDS_DO_NOT_MATCH,
    EMAIL_MAX_LENGTH, INVALID_EMAIL, EMAIL_EXISTS,
    USERNAME_MIN_LENGTH, USERNAME_MAX_LENGTH, USERNAME_CAN_NOT_BE_EMAIL, USERNAME_NO_SPACES, USERNAME_EXISTS
}