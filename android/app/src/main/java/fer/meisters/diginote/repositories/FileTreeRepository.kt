package fer.meisters.diginote.repositories

import android.util.Log
import fer.meisters.diginote.database.db_connection
import fer.meisters.diginote.database.models.Directories
import fer.meisters.diginote.database.models.Directory
import fer.meisters.diginote.database.models.Notebook
import fer.meisters.diginote.database.models.Notebooks
import fer.meisters.diginote.models.FileTree
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext
import org.jetbrains.exposed.dao.flushCache
import org.jetbrains.exposed.sql.transactions.transaction
import org.jetbrains.exposed.sql.update
import java.util.*

object FileTreeRepository {
    suspend fun fileTree(root_uuid: UUID): FileTree {
        return FileTree(createDirectoryNode(root_uuid, null))
    }

    private suspend fun createDirectoryNode(
        uuid: UUID,
        parent: FileTree.Node?
    ): FileTree.Node {
        val node = FileTree.Node(parent, mutableListOf(), mutableListOf(), directory(uuid), null)

        for (directory in subdirectories(uuid)) {
            node.directories.add(createDirectoryNode(directory.id.value, node))
        }

        for (notebook in notebooks(uuid)) {
            node.notebooks.add(createNotebookNode(notebook.id.value, node))
        }

        return node
    }

    private suspend fun createNotebookNode(
        uuid: UUID,
        parent: FileTree.Node?
    ): FileTree.Node {
        return FileTree.Node(parent, mutableListOf(), mutableListOf(), null, notebook(uuid))
    }

    private suspend fun subdirectories(directoryUUID: UUID): List<Directory> {
        return withContext(Dispatchers.IO) {
            transaction(db_connection()) {
                Directory.find { Directories.parentUUID eq directoryUUID }.toList()
            }
        }
    }

    private suspend fun notebooks(directoryUUID: UUID): List<Notebook> {
        return withContext(Dispatchers.IO) {
            transaction(db_connection()) {
                Notebook.find { Notebooks.directoryUUID eq directoryUUID }.toList()
            }
        }
    }

    private suspend fun directory(uuid: UUID): Directory? {
        return withContext(Dispatchers.IO) {
            transaction(db_connection()) {
                Directory.findById(uuid)
            }
        }
    }

    private suspend fun notebook(uuid: UUID): Notebook? {
        return withContext(Dispatchers.IO) {
            transaction(db_connection()) {
                Notebook.findById(uuid)
            }
        }
    }

    private fun getNotebookParent(node: FileTree.Node, notebook: Notebook): FileTree.Node? {
        for (note in node.notebooks) {
            if (note.notebook == notebook) return node
        }

        for (dir in node.directories) {
            val result = getNotebookParent(dir, notebook)
            if (result != null) return result
        }

        return null
    }

    suspend fun moveNotebook(fileTree: FileTree, node: FileTree.Node, notebook: Notebook) {
        withContext(Dispatchers.IO) {
            val parent = getNotebookParent(fileTree.root, notebook)!!
            for (note in parent.notebooks) {
                if (note.notebook == notebook) {
                    node.notebooks.add(note)
                    parent.notebooks.remove(note)
                    break
                }
            }
            transaction(db_connection()) {
                Notebooks.update({ Notebooks.id eq notebook.id }) {
                    it[directoryUUID] = node.directory!!.id.value
                }
            }
        }
    }

    suspend fun renameDirectory(name: String, directory: Directory) {
        withContext(Dispatchers.IO) {
            transaction(db_connection()) {
                directory.name = name
                Directories.update({ Directories.id eq directory.id }) {
                    it[Directories.name] = name
                }
            }
        }
    }

    suspend fun renameNotebook(name: String, notebook: Notebook) {
        withContext(Dispatchers.IO) {
            transaction(db_connection()) {
                notebook.name = name
                Notebooks.update({ Notebooks.id eq notebook.id }) {
                    it[Notebooks.name] = name
                }
            }
        }
    }

    suspend fun deleteDirectory(node: FileTree.Node, directory: Directory) {
        withContext(Dispatchers.IO) {
            for (dir in node.directories) {
                if (directory == dir.directory) {
                    deleteDirectoryChildren(dir)
                    node.directories.remove(dir)

                    transaction(db_connection()) {
                        dir.directory.delete()
                    }

                    break
                }
            }
        }
    }

    suspend fun deleteNotebook(node: FileTree.Node, notebook: Notebook) {
        withContext(Dispatchers.IO) {
            for (note in node.notebooks) {
                if (notebook == note.notebook) {
                    node.notebooks.remove(note)

                    transaction(db_connection()) {
                        note.notebook.delete()
                    }

                    break
                }
            }
        }
    }

    private suspend fun deleteDirectoryChildren(node: FileTree.Node) {
        withContext(Dispatchers.IO) {
            for (dir in node.directories)
                deleteDirectoryChildren(dir)

            transaction(db_connection()) {
                for (dir in node.directories)
                    dir.directory!!.delete()
                for (note in node.notebooks)
                    note.notebook!!.delete()
            }
        }
    }

    suspend fun createDirectory(name: String, directory: FileTree.Node, userUUID: UUID) {
        withContext(Dispatchers.IO) {
            val addedDirectory = transaction(db_connection()) {
                Directory.new {
                    this.parentUUID = directory.directory!!.id.value
                    this.name = name
                    this.userUUID = userUUID
                }
            }

            directory.directories.add(
                FileTree.Node(
                    directory,
                    mutableListOf(),
                    mutableListOf(),
                    addedDirectory,
                    null
                )
            )
        }
    }

    suspend fun createNotebook(name: String, directory: FileTree.Node, userUUID: UUID) {
        withContext(Dispatchers.IO) {
            val addedNotebook = transaction(db_connection()) {
                Notebook.new {
                    this.directoryUUID = directory.directory!!.id.value
                    this.userUUID = userUUID
                    this.name = name
                    this.public = false
                }
            }

            directory.notebooks.add(
                FileTree.Node(
                    directory,
                    mutableListOf(),
                    mutableListOf(),
                    null,
                    addedNotebook
                )
            )
        }
    }

    suspend fun downloadNotebook(notebook: Notebook, rootUUID: UUID): UUID? {
        return withContext(Dispatchers.IO) {
            val id = transaction(db_connection()) {
                Notebook.new {
                    this.directoryUUID = rootUUID
                    this.name = notebook.name
                    this.originalUUID = notebook.id.value
                    this.userUUID = notebook.userUUID
                    this.public = false
                }.id
            }

            val notebookContent = NotebookRepository.fetchContent(notebook.id.value, "token")
            NotebookRepository.updateContent(id.value, notebookContent, "token")
            notebook.id.value
        }
    }

    suspend fun toggleNotebookVisibility(notebook: Notebook) {
        withContext(Dispatchers.IO) {
            transaction(db_connection()) {
                notebook.public = !notebook.public
                Notebooks.update({ Notebooks.id eq notebook.id }) {
                    it[public] = notebook.public
                }
            }
        }
    }
}