package fer.meisters.diginote.database.models

import org.jetbrains.exposed.dao.UUIDEntity
import org.jetbrains.exposed.dao.UUIDEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.UUIDTable
import java.util.*

object Directories : UUIDTable() {
    val name = varchar("name", 25).nullable()
    val parentUUID = uuid("parent_uuid").nullable()
    val userUUID = uuid("user_uuid")
}

class Directory(id: EntityID<UUID>) : UUIDEntity(id) {
    companion object : UUIDEntityClass<Directory>(Directories)

    var name by Directories.name
    var parentUUID by Directories.parentUUID
    var userUUID by Directories.userUUID
}