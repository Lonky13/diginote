package fer.meisters.diginote

import android.text.TextUtils
import android.util.Patterns
import java.math.BigInteger
import java.nio.charset.Charset
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException

fun String.isValidEmail() = !TextUtils.isEmpty(this) && Patterns.EMAIL_ADDRESS.matcher(this).matches()

fun String.md5(): String {
    val digest: MessageDigest
    try {
        digest = MessageDigest.getInstance("MD5")
        digest.update(this.toByteArray(Charset.forName("US-ASCII")), 0, this.length)
        val magnitude: ByteArray = digest.digest()
        val bi = BigInteger(1, magnitude)
        return java.lang.String.format("%0" + (magnitude.size shl 1) + "x", bi)
    } catch (e: NoSuchAlgorithmException) {
        e.printStackTrace()
    }
    return ""
}

