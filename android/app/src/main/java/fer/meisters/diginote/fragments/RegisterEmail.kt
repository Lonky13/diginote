package fer.meisters.diginote.fragments

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import fer.meisters.diginote.databinding.FragmentRegisterEmailBinding

class RegisterEmail : Fragment(), TextWatcher {
    private var callback: OnEmailChangedListener? = null

    private var _binding: FragmentRegisterEmailBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentRegisterEmailBinding.inflate(inflater, container, false)
        binding.registerEmail.editText?.addTextChangedListener(this)
        return binding.root
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        callback = context as? OnEmailChangedListener
        if (callback == null) {
            throw ClassCastException("$context must implement OnArticleSelectedListener")
        }
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
    override fun afterTextChanged(s: Editable?) {
        callback?.onEmailChanged(binding.registerEmail.editText?.text.toString(), binding)
    }

    interface OnEmailChangedListener {
        fun onEmailChanged(email: String, binding: FragmentRegisterEmailBinding)
    }
}  