package fer.meisters.diginote.ui

import androidx.compose.foundation.Canvas
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.viewinterop.viewModel
import fer.meisters.diginote.viewmodels.PageViewModel
import java.util.*
import kotlin.math.sqrt
import android.graphics.Path
import androidx.compose.ui.gesture.*
import androidx.compose.ui.graphics.StrokeJoin
import androidx.compose.ui.graphics.asComposePath
import fer.meisters.diginote.models.Color
import fer.meisters.diginote.viewmodels.unitMatrix

@Composable
fun PageCanvas(viewModel: PageViewModel = viewModel()) {
    val view by viewModel.view.observeAsState(initial = unitMatrix())
    val path by viewModel.path.observeAsState(initial = mutableListOf())
    val initialized by viewModel.initialized.observeAsState(initial = false)
    val color by viewModel.color.observeAsState(initial = Color.White)
    val grid by viewModel.grid.observeAsState(initial = PageViewModel.GridType.BLANK)

    Canvas(
        modifier = Modifier
            .fillMaxSize()
            .background(color.asComposeColor())
            .scaleGestureFilter(scaleObserver = viewModel)
            .dragGestureFilter(dragObserver = viewModel, startDragImmediately = true)
    ) {
        if (!initialized) {
            viewModel.setBounds(size.width, size.height)
            viewModel.fetchContent()
        }

        val max = size.width * sqrt(2.0f)
        var i = size.width * sqrt(2.0f)
        val steps = 50

        val gridPath = Path()
        when (grid) {
            PageViewModel.GridType.SQUARES -> {
                while (i > 0f) {
                    i -= max / steps
                    gridPath.moveTo(0f, i)
                    gridPath.lineTo(size.width, i)
                }
                var j = size.width
                while (j > 0f) {
                    j -= size.width / steps * sqrt(2f)
                    gridPath.moveTo(j, 0f)//samo za hor
                    gridPath.lineTo(j, size.width * sqrt(2f))//samo za hor
                }
            }
            PageViewModel.GridType.LINES -> {
                while (i > 0f) {
                    i -= max / steps
                    gridPath.moveTo(0f, i)
                    gridPath.lineTo(size.width, i)
                }
            }
            PageViewModel.GridType.DOTS -> {
                while (i > 0f) {
                    i -= max / steps

                    var j = size.width
                    while (j > 0f) {
                        j -= size.width / steps
                        gridPath.addCircle(j, i, 3f, Path.Direction.CW)
                    }
                }
            }
        }

        gridPath.transform(view)
        drawPath(
            gridPath.asComposePath(),
            androidx.compose.ui.graphics.Color.Black,
            alpha = 1f,
            style = Stroke(width = 2.0f, join = StrokeJoin.Round)
        )

        for (pathPart in path) {
            val transformedPath = Path()
            transformedPath.addPath(pathPart.second)
            transformedPath.transform(view)

            drawPath(
                transformedPath.asComposePath(),
                pathPart.first.second.asComposeColor(),
                alpha = 1f,
                style = Stroke(width = pathPart.first.first, join = StrokeJoin.Round)
            )
        }
    }
}