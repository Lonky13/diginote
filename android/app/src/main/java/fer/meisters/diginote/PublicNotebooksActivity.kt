package fer.meisters.diginote

import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.compose.foundation.ScrollableColumn
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.material.ripple.rememberRipple
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.savedinstancestate.savedInstanceState
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.scale
import androidx.compose.ui.focus
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.AmbientContext
import androidx.compose.ui.platform.setContent
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.viewinterop.viewModel
import androidx.compose.ui.window.Dialog
import fer.meisters.diginote.database.models.Notebook
import fer.meisters.diginote.ui.AppTheme
import fer.meisters.diginote.ui.NavigationBar
import fer.meisters.diginote.viewmodels.HomeViewModel
import fer.meisters.diginote.viewmodels.PublicNotebooksViewModel
import io.jsonwebtoken.Jwts
import java.util.*

class PublicNotebooksActivity : AppCompatActivity() {

    companion object {
        const val TAG = "PublicNotebooksActivity"
    }

    private lateinit var userUUID: UUID
    private lateinit var rootUUID: UUID

    private val viewModel: PublicNotebooksViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.i(TAG, "Activity created.")

        setContent {
            AppTheme {
                Scaffold(topBar = {
                    ContextualSearchBar()
                }, bodyContent = { SearchResults(userUUID, rootUUID) })
            }
        }

        // Extract user info from jwt token
        val userPreferences: SharedPreferences = getSharedPreferences("user", MODE_PRIVATE)
        val userInfo = Jwts.parser().setSigningKey(BuildConfig.JWT_SECRET.toByteArray())
            .parseClaimsJws(
                userPreferences.getString("jwt_token", "This user does not exist!")
            ).body

        userUUID = UUID.fromString(userInfo["uuid"] as String)
        rootUUID = UUID.fromString(userInfo["rootUUID"] as String)
    }

    override fun onBackPressed() {
        if (!viewModel.searchMode.value!!)
            super.onBackPressed()
        else
            viewModel.exitSearchMode()
    }
}

@Composable
private fun ContextualSearchBar(viewModel: PublicNotebooksViewModel = viewModel()) {
    val context = AmbientContext.current

    val searchMode by viewModel.searchMode.observeAsState(initial = false)

    if (searchMode) {
        SearchBar()
    } else {
        NavigationBar(
            "Explore",
            Icons.Filled.Search,
            Icons.Filled.Home,
            {
                viewModel.enterSearchMode()
            },
            {
                context.startActivity(
                    Intent(
                        context,
                        HomeActivity::class.java
                    )
                )
            }
        )
    }
}

@Composable
private fun SearchBar(viewModel: PublicNotebooksViewModel = viewModel()) {
    var text by savedInstanceState { "" }
    TextField(
        modifier = Modifier.fillMaxWidth().height(65.dp),
        backgroundColor = MaterialTheme.colors.secondary,
        value = text,
        singleLine = true,
        activeColor = MaterialTheme.colors.onSurface,
        onValueChange = {
            text = if (it.length <= 20) it else text
            viewModel.search(text)
        },
        leadingIcon = {
            Icon(
                imageVector = Icons.Filled.Search,
                tint = MaterialTheme.colors.onSecondary
            )
        },
        label = { Text("Search") }
    )
}

@Composable
private fun SearchResults(
    userUUID: UUID,
    rootUUID: UUID,
    viewModel: PublicNotebooksViewModel = viewModel()
) {
    val searchResults by viewModel.searchResults.observeAsState(initial = null)

    if (searchResults == null) {
        Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
            Text(text = "Try searching for something or someone.", color = Color.Gray)
        }
        return
    }

    if (searchResults!!.isEmpty()) {
        Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
            Text(text = "No results.", color = Color.Gray)
        }
        return
    }

    ScrollableColumn(
        modifier = Modifier.fillMaxSize().padding(10.dp),
        verticalArrangement = Arrangement.spacedBy(10.dp),
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {

        for (result in searchResults!!) {
            Card(
                shape = RoundedCornerShape(4.dp),
                backgroundColor = MaterialTheme.colors.secondaryVariant,
                modifier = Modifier.fillMaxWidth().height(100.dp)
            ) {
                Row(
                    horizontalArrangement = Arrangement.SpaceBetween,
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Row(
                        horizontalArrangement = Arrangement.Start,
                        verticalAlignment = Alignment.CenterVertically,
                        modifier = Modifier.weight(0.8f)
                    )
                    {
                        Icon(
                            imageVector = Icons.Filled.MenuBook,
                            tint = MaterialTheme.colors.onSecondary,
                            modifier = Modifier.fillMaxHeight().padding(20.dp)
                        )
                        Column(
                            verticalArrangement = Arrangement.SpaceAround,
                            horizontalAlignment = Alignment.Start
                        ) {
                            Text(
                                text = result.second.name,
                                color = MaterialTheme.colors.onSecondary
                            )

                            Text(
                                text = result.first.username,
                                color = MaterialTheme.colors.onSecondary,
                                fontSize = 12.sp
                            )
                        }
                    }
                    Icon(
                        imageVector = Icons.Filled.GetApp,
                        tint = MaterialTheme.colors.onSecondary,
                        modifier = Modifier.fillMaxHeight().weight(0.2f)
                            .clickable(
                                onClick = {
                                    viewModel.downloadNotebook(
                                        result.second,
                                        rootUUID
                                    )
                                },
                                indication = rememberRipple(bounded = true)
                            )
                    )
                }
            }
        }
    }
}