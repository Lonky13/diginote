package fer.meisters.diginote.ui

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.ripple.rememberRipple
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.unit.Dp

@Composable
fun NavigationBar(
    title: String,
    left: ImageVector,
    right: ImageVector,
    leftOnClick: () -> Unit,
    rightOnClick: () -> Unit
) {
    val height = Dp(65f)
    Row(
        horizontalArrangement = Arrangement.SpaceBetween,
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier.fillMaxWidth()
            .height(height)
            .background(
                MaterialTheme.colors.primary
            )
    ) {
        NavigationButton(icon = left, onClick = leftOnClick)
        Text(text = title, color = MaterialTheme.colors.onPrimary)
        NavigationButton(icon = right, onClick = rightOnClick)
    }
}

@Composable
fun NavigationButton(icon: ImageVector, onClick: () -> Unit) {
    Box(
        modifier = Modifier.fillMaxHeight().clickable(
            onClick = onClick,
            indication = rememberRipple(bounded = true)
        )
    ) {
        Icon(
            imageVector = icon,
            tint = MaterialTheme.colors.onPrimary,
            modifier = Modifier.fillMaxHeight().padding(vertical = Dp(20f), horizontal = Dp(40f))
        )
    }
}