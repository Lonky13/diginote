package fer.meisters.diginote.models

import android.graphics.PointF
import com.google.gson.annotations.SerializedName
import fer.meisters.diginote.viewmodels.PageViewModel

enum class Color {
    LightGray, DarkGray, Red, White, Blue, Green, Black;

    fun asComposeColor() : androidx.compose.ui.graphics.Color {
        return when (this) {
            LightGray -> {
                androidx.compose.ui.graphics.Color.LightGray
            }
            DarkGray -> {
                androidx.compose.ui.graphics.Color.DarkGray
            }
            Red -> {
                androidx.compose.ui.graphics.Color.Red
            }
            White -> {
                androidx.compose.ui.graphics.Color.White
            }
            Blue -> {
                androidx.compose.ui.graphics.Color.Blue
            }
            Green -> {
                androidx.compose.ui.graphics.Color.Green
            }
            Black -> {
                androidx.compose.ui.graphics.Color.Black
            }
        }
    }
}

data class PageModel(
    @SerializedName("position") var position: Int,
    @SerializedName("textPath") val textPath: MutableList<Pair<Pair<Float, Color>, MutableList<Pair<PointF, PointF>>>> = mutableListOf(),
    @SerializedName("code") var code: String = "# Type your python code here",
    @SerializedName("drawingPath") val drawingPath: MutableList<Pair<Pair<Float, Color>, MutableList<Pair<PointF, PointF>>>> = mutableListOf(),
    @SerializedName("color") var color: Color = Color.White,
    @SerializedName("grid") var grid: PageViewModel.GridType = PageViewModel.GridType.BLANK
//    @Expose (serialize = false, deserialize = false)
)