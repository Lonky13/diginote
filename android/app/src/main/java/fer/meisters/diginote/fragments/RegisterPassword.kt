package fer.meisters.diginote.fragments

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import fer.meisters.diginote.databinding.FragmentRegisterPasswordBinding

class RegisterPassword : Fragment(), TextWatcher {
    private var callbackPassword: OnPasswordChangedListener? = null
    private var callbackPasswordConfirm: OnPasswordConfirmChangedListener? = null

    private var _binding: FragmentRegisterPasswordBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentRegisterPasswordBinding.inflate(inflater, container, false)

        binding.registerPassword.editText?.addTextChangedListener(this)
        binding.registerConfirmPassword.editText?.addTextChangedListener(this)

        return binding.root
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        callbackPassword = context as? OnPasswordChangedListener
        if (callbackPassword == null) {
            throw ClassCastException("$context must implement OnArticleSelectedListener")
        }
        callbackPasswordConfirm = context as? OnPasswordConfirmChangedListener
        if (callbackPasswordConfirm == null) {
            throw ClassCastException("$context must implement OnArticleSelectedListener")
        }
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
    override fun afterTextChanged(s: Editable?) {
        binding.registerPassword.error = callbackPassword?.onPasswordChanged(binding.registerPassword.editText?.text.toString())
        binding.registerConfirmPassword.error = callbackPasswordConfirm?.onPasswordConfirmChanged(binding.registerConfirmPassword.editText?.text.toString())
    }

    interface OnPasswordChangedListener {
        fun onPasswordChanged(password: String): String?
    }

    interface OnPasswordConfirmChangedListener {
        fun onPasswordConfirmChanged(password_confirm: String): String?
    }
}  