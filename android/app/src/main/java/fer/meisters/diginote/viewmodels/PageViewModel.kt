package fer.meisters.diginote.viewmodels

import android.graphics.Matrix
import android.graphics.Path
import android.graphics.PointF
import android.util.Log
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.gesture.DragObserver
import androidx.compose.ui.gesture.ScaleObserver
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.gson.Gson
import fer.meisters.diginote.md5
import fer.meisters.diginote.models.NotebookModel
import fer.meisters.diginote.models.PageModel
import fer.meisters.diginote.models.Color
import fer.meisters.diginote.repositories.NotebookRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.File
import java.util.*
import kotlin.math.max
import kotlin.math.min
import kotlin.math.sqrt

fun unitMatrix(): Matrix {
    val unit = Matrix()
    unit.setValues(floatArrayOf(1f, 0f, 0f, 0f, 1f, 0f, 0f, 0f, 1f))
    return unit
}

data class Bounds(var top: Float, var bottom: Float, var left: Float, var right: Float)

class PageViewModel : ViewModel(), ScaleObserver, DragObserver {

    val toggleGrid: LiveData<Boolean> get() = mutableToggleGridActive
    private val mutableToggleGridActive = MutableLiveData(false)
    
    val codeMode: LiveData<Boolean> get() = mutableCodeMode
    private val mutableCodeMode = MutableLiveData(false)

    val code: LiveData<String> get() = mutableCode
    private val mutableCode = MutableLiveData("# Type your python code here")

    private val mutableGrid = MutableLiveData(GridType.BLANK)
    val grid: LiveData<GridType> get() = mutableGrid

    private val mutableAddColorActive = MutableLiveData(false)
    val addColorActive: LiveData<Boolean> get() = mutableAddColorActive

    private val mutableStrokeColorActive = MutableLiveData(false)
    val strokeColorActive: LiveData<Boolean> get() = mutableStrokeColorActive

    private val mutableStrokeSizeActive = MutableLiveData(false)
    val strokeSizeActive: LiveData<Boolean> get() = mutableStrokeSizeActive

    private val mutableLoading = MutableLiveData(false)
    val loading: LiveData<Boolean> get() = mutableLoading

    private val mutableOCRResults = MutableLiveData<String>(null)
    val OCRResults: LiveData<String> get() = mutableOCRResults

    private val mutableColor = MutableLiveData(Color.White)
    val color: LiveData<Color> get() = mutableColor

    private var strokeColor = Color.Black
    private var strokeSize = 5f

    var mode: Mode = Mode.TEXT

    // Exposed view matrix
    val view: LiveData<Matrix> get() = mutableView
    private val mutableView = MutableLiveData<Matrix>(unitMatrix())

    // View matrix components
    private var translationMatrix = unitMatrix()
    private var scalingMatrix = unitMatrix()

    // Helper
    val initialized: LiveData<Boolean> get() = mutableInitialized
    private val mutableInitialized = MutableLiveData<Boolean>(false)

    // Path
    val path: LiveData<MutableList<Pair<Pair<Float, Color>, Path>>> get() = mutablePath
    private val mutablePath =
        MutableLiveData<MutableList<Pair<Pair<Float, Color>, Path>>>(mutableListOf())

    private var textPath: MutableList<Pair<Pair<Float, Color>, Path>> = mutableListOf()
    private var drawingPath: MutableList<Pair<Pair<Float, Color>, Path>> = mutableListOf()
    private var codePath: MutableList<Pair<Pair<Float, Color>, Path>> = mutableListOf()

    private var bounds = Bounds(0f, 0f, 0f, 0f)

    private var height = 0f
    private var width = 0f

    private var HEIGHT = 0f
    private var WIDTH = 0f

    private var scalingFactor = 1f

    private lateinit var notebook: NotebookModel
    private lateinit var notebookFile: File
    private var position: Int = -1
    private lateinit var notebookUUID: UUID

    private val pageHistory = mutableListOf<PageModel>()
    private val textHistory = mutableListOf<MutableList<Pair<Pair<Float, Color>, Path>>>()
    private val drawingHistory = mutableListOf<MutableList<Pair<Pair<Float, Color>, Path>>>()

    private var lastPath: Pair<Pair<Float, Color>, Path>? = null

    enum class Mode {
        MOVE, TEXT, DRAW, CODE
    }

    enum class GridType {
        BLANK, DOTS, SQUARES, LINES
    }

    fun setGridType(grid: GridType) {
        mutableGrid.value = grid
        notebook.pages[position].grid = grid
        makeCopy()
    }

    fun setFile(file: File, pos: Int, uuid: UUID) {
        notebookFile = file
        position = pos
        notebookUUID = uuid
    }

    fun setStrokeColor(color: Color) {
        strokeColor = color
    }

    fun setStrokeSize(size: Float) {
        strokeSize = size
    }
    
    fun enterCodeMode(){
        mutableCodeMode.value = true
    }
    
    fun exitCodeMode(){
        mutableCodeMode.value = false
    }

    fun updateCode(code: String){
        mutableCode.value = code
        notebook.pages[position].code = code
    }
    
    fun runCode(){
        GlobalScope.launch(Dispatchers.Main) {
            mutableLoading.postValue(true)
            notebookFile.writeText(Gson().toJson(notebook))
            NotebookRepository.updateContent(notebookUUID, notebook, "token")
            val text = NotebookRepository.Runner(notebookUUID, position, "token")
            mutableLoading.postValue(false)
            mutableOCRResults.postValue(text)
        }
    }

    private fun generatePaths(
        storedPaths: MutableList<Pair<Pair<Float, Color>, MutableList<Pair<PointF, PointF>>>>,
        cachedPaths: MutableList<Pair<Pair<Float, Color>, Path>>
    ) {
        for (path in storedPaths) {
            val pathColor = path.first.second
            val pathSize = path.first.first

            val emptyPath = Pair(Pair(pathSize, pathColor), Path())
            for (node in path.second) {
                emptyPath.second.moveTo(node.first.x * WIDTH, node.first.y * HEIGHT)
                emptyPath.second.lineTo(node.second.x * WIDTH, node.second.y * HEIGHT)
            }
            cachedPaths.add(emptyPath)
        }
    }

    fun fetchContent() {
        GlobalScope.launch(Dispatchers.IO) {
            notebook = Gson().fromJson(notebookFile.readText(), NotebookModel::class.java)
            val page = notebook.pages[position]

            generatePaths(page.textPath, textPath)
            generatePaths(page.drawingPath, drawingPath)

            mutableCode.postValue(page.code)
            mutableGrid.postValue(page.grid)
            mutableColor.postValue(page.color)

            makeCopy()
            updatePath()
        }
    }

    fun updateContent() {
        GlobalScope.launch(Dispatchers.IO) {
            for (position in 0 until notebook.pages.size) {
                notebook.hashes[position] =
                    (notebook.pages[position].code + Gson().toJson(notebook.pages[position].drawingPath) + Gson().toJson(
                        notebook.pages[position].textPath
                    ) + Gson().toJson(notebook.pages[position].grid) + Gson().toJson(notebook.pages[position].color)).md5()
            }
            notebookFile.writeText(Gson().toJson(notebook))
            NotebookRepository.updateContent(notebookUUID, notebook, "token")
        }
    }

    fun setPageColor(color: Color) {
        mutableColor.value = color
        notebook.pages[position].color = color
        makeCopy()
    }

    fun toggleGrid() {
        mutableToggleGridActive.value = !mutableToggleGridActive.value!!
    }

    fun toggleAddColor() {
        mutableAddColorActive.value = !mutableAddColorActive.value!!
    }

    fun toggleStrokeColor() {
        mutableStrokeColorActive.value = !mutableStrokeColorActive.value!!
    }

    fun toggleStrokeSize() {
        mutableStrokeSizeActive.value = !mutableStrokeSizeActive.value!!
    }

    // Scale events
    override fun onScale(scaleFactor: Float) {
        if (mode == Mode.MOVE) {
            val cx = width / 2
            val cy = height / 2

            val right =
                0.5f * (WIDTH - bounds.right - bounds.left) * (1 - 1 / scaleFactor) + bounds.right
            val left =
                0.5f * (WIDTH - bounds.right - bounds.left) * (1 - 1 / scaleFactor) + bounds.left
            val top =
                0.5f * (HEIGHT - bounds.top - bounds.bottom) * (1 - 1 / scaleFactor) + bounds.top
            val bottom =
                0.5f * (HEIGHT - bounds.top - bounds.bottom) * (1 - 1 / scaleFactor) + bounds.bottom

            val sb = Bounds(top, bottom, left, right)

            if (sb.left < 0 || sb.right < 0 || sb.top < 0 || sb.bottom < 0)

                return

            val scaling = Matrix()

            scaling.setScale(scaleFactor, scaleFactor, cx, cy)
            scalingMatrix.postConcat(scaling)
            scalingFactor *= scaleFactor

            bounds = sb

            updateView()
        }
    }

    fun setBounds(width: Float, height: Float) {
        this.height = height
        this.width = width

        WIDTH = width
        HEIGHT = width * sqrt(2.0f)

        bounds = Bounds(0f, HEIGHT - height, 0f, 0f)

        updateView()

        mutableInitialized.value = true
    }

    private fun moveToPath(
        x: Float,
        y: Float,
        storedPath: MutableList<Pair<Pair<Float, Color>, MutableList<Pair<PointF, PointF>>>>,
        memoryPath: MutableList<Pair<Pair<Float, Color>, Path>>
    ) {
        val newPath = Path()
        newPath.moveTo(x, y)

        memoryPath.add(
            Pair(
                Pair(strokeSize, strokeColor),
                newPath
            )
        )
        storedPath.add(
            Pair(
                Pair(strokeSize, strokeColor),
                mutableListOf(
                    Pair(
                        PointF(
                            x / WIDTH,
                            y / HEIGHT
                        ), PointF(0f, 0f)
                    )
                )
            )
        )

        lastPath = memoryPath.last()
    }

    // Drag events
    override fun onStart(downPosition: Offset) {
        if (mode == Mode.TEXT || mode == Mode.CODE || mode == Mode.DRAW) {
            val transformed = floatArrayOf(downPosition.x, downPosition.y)
            val inverse = Matrix()
            view.value!!.invert(inverse)
            inverse.mapPoints(transformed)

            mutablePath.value!!.add(Pair(Pair(0f, Color.Black), Path()))
            when (mode) {
                Mode.TEXT -> {
                    moveToPath(
                        transformed[0],
                        transformed[1],
                        notebook.pages[position].textPath,
                        textPath
                    )
                }
                Mode.DRAW -> {
                    moveToPath(
                        transformed[0],
                        transformed[1],
                        notebook.pages[position].drawingPath,
                        drawingPath
                    )
                }
            }

            updatePath()
        }
    }

    private fun relativeLineTo(
        x: Float,
        y: Float,
        storedPath: MutableList<Pair<Pair<Float, Color>, MutableList<Pair<PointF, PointF>>>>,
        memoryPath: MutableList<Pair<Pair<Float, Color>, Path>>
    ) {
        memoryPath.last().second.rLineTo(
            x * 1 / scalingFactor,
            y * 1 / scalingFactor
        )
        storedPath.last().second.last().second.x =
            storedPath.last().second.last().first.x + x * 1 / scalingFactor / WIDTH
        storedPath.last().second.last().second.y =
            storedPath.last().second.last().first.y + y * 1 / scalingFactor / HEIGHT

        storedPath.last().second.add(
            Pair(
                PointF(
                    storedPath.last().second.last().second.x,
                    storedPath.last().second.last().second.y
                ), PointF(0f, 0f)
            )
        )

        lastPath = memoryPath.last()
    }

    override fun onDrag(dragDistance: Offset): Offset {
        if (mode == Mode.MOVE) {
            val translation = Matrix()

            val translateX = max(min(dragDistance.x, bounds.right), -bounds.left)
            val translateY = max(
                min(dragDistance.y, bounds.top),
                -bounds.bottom
            )

            translation.setTranslate(
                translateX * 1 / scalingFactor,
                translateY * 1 / scalingFactor
            )
            translationMatrix.postConcat(translation)

            bounds.right -= translateX / scalingFactor
            bounds.left += translateX / scalingFactor
            bounds.top -= translateY / scalingFactor
            bounds.bottom += translateY / scalingFactor

            updateView()

        } else if (mode == Mode.TEXT || mode == Mode.DRAW || mode == Mode.CODE) {
            when (mode) {
                Mode.TEXT -> {
                    relativeLineTo(
                        dragDistance.x,
                        dragDistance.y,
                        notebook.pages[position].textPath,
                        textPath
                    )
                }
                Mode.DRAW -> {
                    relativeLineTo(
                        dragDistance.x,
                        dragDistance.y,
                        notebook.pages[position].drawingPath,
                        drawingPath
                    )
                }
            }

            updatePath()
        }

        return dragDistance
    }

    override fun onStop(velocity: Offset) {
        when (mode) {
            Mode.TEXT -> {
                notebook.pages[position].textPath.last().second.removeLast()
            }
            Mode.DRAW -> {
                notebook.pages[position].drawingPath.last().second.removeLast()
            }
        }

        if (mode == Mode.TEXT || mode == Mode.DRAW || mode == Mode.CODE) {
            makeCopy()
            updatePath(true)
        }
    }

    private fun makeCopy() {
        pageHistory.add(
            PageModel(
                position = position,
                textPath = notebook.pages[position].textPath.toMutableList(),
                code = mutableCode.value + "",
                drawingPath = notebook.pages[position].drawingPath.toMutableList(),
                color = notebook.pages[position].color,
                grid = notebook.pages[position].grid
            )

        )

        textHistory.add(textPath.toMutableList())
        drawingHistory.add(drawingPath.toMutableList())
    }

    fun undo() {
        if (pageHistory.size > 1) {
            pageHistory.removeLast()
            textHistory.removeLast()
            drawingHistory.removeLast()

            notebook.pages[position] =
                PageModel(
                    position = position,
                    textPath = pageHistory.last().textPath.toMutableList(),
                    code = mutableCode.value + "",
                    drawingPath = pageHistory.last().drawingPath.toMutableList(),
                    color = pageHistory.last().color,
                    grid = pageHistory.last().grid
                )

            textPath = textHistory.last().toMutableList()
            drawingPath = drawingHistory.last().toMutableList()

            mutableColor.value = notebook.pages[position].color
            mutableGrid.value = notebook.pages[position].grid

            val path: MutableList<Pair<Pair<Float, Color>, Path>> = mutableListOf()
            path.addAll(textPath)
            path.addAll(codePath)
            path.addAll(drawingPath)

            mutablePath.value = path.toMutableList()
        }
    }

    private fun updateView() {
        val view = Matrix(translationMatrix)
        view.postConcat(scalingMatrix)

        mutableView.value = Matrix(view)
    }

    private fun updatePath(finish: Boolean = false) {
        val path: MutableList<Pair<Pair<Float, Color>, Path>> =
            mutablePath.value!!.toMutableList()

        if (path.isEmpty() && lastPath == null) {
            path.addAll(textPath)
            path.addAll(codePath)
            path.addAll(drawingPath)
        } else if (lastPath != null) {
            if (path.isNotEmpty())
                path.removeLast()
            path.add(Pair(lastPath!!.first, Path(lastPath!!.second)))

            if (finish) lastPath = null
        }

        mutablePath.postValue(path.toMutableList())
    }

    override fun onCancel() {
        TODO("Not yet implemented")
    }

    fun OCR() {
        GlobalScope.launch(Dispatchers.Main) {
            mutableLoading.postValue(true)
            notebookFile.writeText(Gson().toJson(notebook))
            NotebookRepository.updateContent(notebookUUID, notebook, "token")
            val text = NotebookRepository.OCR(notebookUUID, position, "token")
            mutableLoading.postValue(false)
            mutableOCRResults.postValue(text)
        }
    }

    fun exitOCRResults() {
        mutableOCRResults.value = null
    }
}