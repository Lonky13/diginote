package fer.meisters.diginote.ui

import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.savedinstancestate.savedInstanceState
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.viewModel
import androidx.compose.ui.window.Dialog
import fer.meisters.diginote.viewmodels.HomeViewModel
import java.util.*

@Composable
fun ConfirmationDialog(prompt: String, onConfirm: () -> Unit, onDecline: () -> Unit) {
    Dialog(onDismissRequest = { }) {
        Card {
            Column(horizontalAlignment = Alignment.CenterHorizontally) {
                Text(text = prompt, modifier = Modifier.padding(20.dp, 20.dp, 20.dp, 0.dp), textAlign = TextAlign.Center)
                Row(
                    horizontalArrangement = Arrangement.SpaceEvenly,
                    modifier = Modifier.padding(20.dp).fillMaxWidth()
                ) {
                    TextButton(
                        onClick = onConfirm) {
                        Text(text = "Yes", color = MaterialTheme.colors.onSurface)
                    }
                    TextButton(onClick = onDecline) {
                        Text(text = "No", color = MaterialTheme.colors.onSurface)
                    }
                }
            }
        }
    }
}