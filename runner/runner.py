import zmq
from subprocess import STDOUT, check_output, TimeoutExpired

context = zmq.Context()
socket = context.socket(zmq.REP)
socket.bind("tcp://0.0.0.0:5556")

print("Runner is up.")

while True:
    code = socket.recv()

    with open("code.py", "w") as code_file:
        code_file.write(code.decode("utf-8"))

    command = ['python', 'code.py']
    
    try:
        output = check_output(command, stderr=STDOUT, timeout=5)

        print(f"Runner:\n{output}")
        socket.send_string(output.decode())
    except TimeoutExpired:
        print(f"Runner timeout.")
        socket.send_string("Execution timeout.")
    except Exception as e:
        print(f"Runner internal error.")
        socket.send_string(f"Error. Check your syntax.")
