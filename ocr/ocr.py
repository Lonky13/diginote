import json
import math
import numpy as np
import cv2 as cv
import zmq

import easyocr

context = zmq.Context()
socket = context.socket(zmq.REP)
socket.bind("tcp://0.0.0.0:5555")

reader = easyocr.Reader(['en'], gpu = False) 

while True:
    paths = socket.recv()

    data_dict = json.loads(paths)

    width = 1500
    height = int(width * math.sqrt(2))
    canvas = np.zeros((width, height, 1))

    for path in data_dict:
        for segment in path["second"]:
            start = segment["first"]
            end = segment["second"]

            x1 = int(start["x"] * width)
            y1 = int(start["y"] * width)

            x2 = int(end["x"] * width)
            y2 = int(end["y"] * width)

            cv.line(canvas, pt1=(x1,y1), pt2=(x2,y2), thickness=5, color=(255))

    coords = cv.findNonZero(canvas)
    x, y, w, h = cv.boundingRect(coords)
    rect = canvas[y:y+h, x:x+w]

    rect = cv.copyMakeBorder(rect, 100, 100, 100, 100, cv.BORDER_CONSTANT, None, (0))
    rect = (255-rect)
    cv.imwrite('img.jpg', rect)


    result = reader.readtext('img.jpg')

    text = ''
    for coords, chars, prob in result:
        text += ' ' + chars

    print("Extracted: " + text.strip())
    socket.send_string(text.strip())
