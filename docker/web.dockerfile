FROM node:latest

COPY ./web /app/web

WORKDIR /app/web

RUN npm install
RUN npm install -g @babel/cli @babel/core
RUN npx babel ./jsx --out-dir ./js --source-maps --copy-files

ENTRYPOINT node src/index.js
