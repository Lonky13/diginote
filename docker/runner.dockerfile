FROM python:3.7

COPY runner/requirements.txt requirements.txt
RUN pip install --upgrade pip
RUN pip install -r requirements.txt

WORKDIR /app/runner

ENTRYPOINT python runner.py
