import pool from "../database.js";
import * as fs from "fs";

import zmq from "zeromq";

const API = function () {};

API.prototype.userSearch = async function (search) {
    const res = await pool.query(
        `SELECT id, email, username, role FROM users WHERE email LIKE '%${search}%' OR username LIKE '%${search}%' LIMIT 10`
    );

    return Promise.resolve(res.rows);
};

API.prototype.userInfo = async function (uuid) {
    const res = await pool.query(
        `SELECT id, email, username, role FROM users WHERE id = '${uuid}'`
    );

    return Promise.resolve(res.rows[0]);
};

API.prototype.updateUser = async function (uuid, email, username, role) {
    const res = await pool.query(
        `UPDATE users SET email='${email}', username='${username}', role='${role}' WHERE id='${uuid}'`
    );

    return Promise.resolve('["OK"]');
};

API.prototype.getNotebookContent = async function (uuid) {
    let fileContents = null;
    try {
        if (!fs.existsSync("data")) fs.mkdirSync("data");
        if (!fs.existsSync("data/notebooks")) fs.mkdirSync("data/notebooks");

        if (!fs.existsSync("data/notebooks/" + uuid))
            fs.writeFileSync(
                "data/notebooks/" + uuid,
                '{"pages" : [], "hashes" : [] }'
            );

        fileContents = fs.readFileSync("data/notebooks/" + uuid).toString();
    } catch (err) {
        console.log(err);
    }

    return Promise.resolve(fileContents);
};

API.prototype.updateNotebookContent = async function (uuid, content) {
    let fileContents = null;
    try {
        if (!fs.existsSync("data")) fs.mkdirSync("data");
        if (!fs.existsSync("data/notebooks")) fs.mkdirSync("data/notebooks");

        fs.writeFileSync("data/notebooks/" + uuid, JSON.stringify(content));
    } catch (err) {
        console.log(err);
    }

    return Promise.resolve('["OK"]');
};

API.prototype.OCR = async function (uuid, position) {
    const sock = new zmq.Request();
    sock.connect("tcp://ocr:5555");

    let text = "";
    try {
        if (!fs.existsSync("data")) fs.mkdirSync("data");
        if (!fs.existsSync("data/notebooks")) fs.mkdirSync("data/notebooks");

        if (!fs.existsSync("data/notebooks/" + uuid))
            fs.writeFileSync(
                "data/notebooks/" + uuid,
                '{"pages" : [], "hashes" : [] }'
            );

        const fileContents = JSON.parse(
            fs.readFileSync("data/notebooks/" + uuid).toString()
        );

        await sock.send(
            JSON.stringify(fileContents["pages"][Number(position)]["textPath"])
        );
        const [result] = await sock.receive();
        text = result.toString();
        console.log("OCR response: " + text);
    } catch (err) {
        console.log(err);
    }

    return Promise.resolve(text);
};

API.prototype.Runner = async function (uuid, position) {
    const sock = new zmq.Request();
    sock.connect("tcp://runner:5556");

    let text = "";
    try {
        if (!fs.existsSync("data")) fs.mkdirSync("data");
        if (!fs.existsSync("data/notebooks")) fs.mkdirSync("data/notebooks");

        if (!fs.existsSync("data/notebooks/" + uuid))
            fs.writeFileSync(
                "data/notebooks/" + uuid,
                '{"pages" : [], "hashes" : [] }'
            );

        const fileContents = JSON.parse(
            fs.readFileSync("data/notebooks/" + uuid).toString()
        );

        await sock.send(fileContents["pages"][Number(position)]["code"]);
        const [result] = await sock.receive();
        text = result.toString();
        console.log("Runner response:\n" + text);
    } catch (err) {
        console.log(err);
    }

    return Promise.resolve(text);
};

export default API;
