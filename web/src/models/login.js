import pg from "pg";
import bcrypt from "bcrypt";
import pool from "../database.js";

const Login = function (data) {
    this.data = data;
};

Login.prototype.validate = async function () {
    const { email, password } = this.data;

    const res = await pool.query("SELECT * FROM users WHERE email = $1", [email]);

    if (res.rows.length === 0) {
        return Promise.reject("User with that email doesn't exist.");
    } else {
        if (
            (await bcrypt.compare(password, res.rows[0].hashed_password)) === false
        ) {
            return Promise.reject("Wrong password.");
        } else {
            if (res.rows[0].role === 0) {
                return Promise.reject(
                    "You do not have sufficient rights to access the admin dashboard."
                );
            }
        }
    }

    return Promise.resolve(res.rows[0]);
};

export default Login;
