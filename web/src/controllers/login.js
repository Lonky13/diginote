import jwt from "jsonwebtoken";
import Login from "../models/login.js";

const renderLogin = (req, res) => {
    if (
        req.cookies["jwt_token"] !== undefined &&
    jwt.verify(req.cookies["jwt_token"], process.env.JWT_SECRET)
    )
        res.redirect("dashboard");
    else res.render("login");
};

const postLogin = (req, res) => {
    const login = new Login(req.body);
    login
        .validate()
        .then((user) => {
            res.cookie(
                "jwt_token",
                jwt.sign(
                    {
                        uuid: user.id,
                        username: user.username,
                        email: user.email,
                        role: user.role,
                    },
                    process.env.JWT_SECRET
                ),
                { httpOnly: true }
            );
            res.redirect("/dashboard");
        })
        .catch((error) => {
            res.render("login", {
                error: error,
            });
        });
};

export { renderLogin, postLogin };
