import jwt from "jsonwebtoken";

const renderDashboard = (req, res) => {
    if (
        req.cookies["jwt_token"] !== undefined &&
    jwt.verify(req.cookies["jwt_token"], process.env.JWT_SECRET)
    )
        res.render("dashboard");
    else res.status(403).send("Forbidden.");
};

export { renderDashboard };
