import jwt from "jsonwebtoken";
import User from "../models/user.js";

const renderEditUser = (req, res) => {
    if (
        req.cookies["jwt_token"] !== undefined &&
    jwt.verify(req.cookies["jwt_token"], process.env.JWT_SECRET)
    )
        res.render("edit");
    else res.status(403).send("Forbidden.");
};

export { renderEditUser };
