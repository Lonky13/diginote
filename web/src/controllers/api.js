import jwt from "jsonwebtoken";
import API from "../models/api.js";

const apiUserSearch = (req, res) => {
    const api = new API();

    api.userSearch(req.params.search).then((users) => {
        res.send(users);
    });
};

const apiUserInfo = (req, res) => {
    const api = new API();

    api.userInfo(req.params.uuid).then((user) => {
        res.send(user);
    });
};

const apiUpdateUser = (req, res) => {
    const api = new API();

    api.updateUser(
        req.params.uuid,
        req.params.email,
        req.params.username,
        req.params.role
    ).then((state) => {
        res.send(state);
    });
};

const apiGetNotebookContent = (req, res) => {
    const api = new API();

    api.getNotebookContent(req.params.uuid).then((content) => {
        res.setHeader("Content-Type", "application/json").send(content);
    });
};

const apiUpdateNotebookContent = (req, res) => {
    const api = new API();

    api.updateNotebookContent(req.params.uuid, req.body).then((state) => {
        res.send(state);
    });
};

const apiOCR = (req, res) => {
    const api = new API();

    api.OCR(req.params.uuid, req.params.position).then((content) => {
        res.setHeader("Content-Type", "text/plain").send(content);
    });
};

const apiRunner = (req, res) => {
    const api = new API();

    api.Runner(req.params.uuid, req.params.position).then((content) => {
        res.setHeader("Content-Type", "text/plain").send(content);
    });
};

export {
    apiUserSearch,
    apiUserInfo,
    apiUpdateUser,
    apiGetNotebookContent,
    apiUpdateNotebookContent,
    apiOCR,
    apiRunner,
};
