import express from "express";
import { renderLogin, postLogin } from "./controllers/login.js";
import { renderDashboard } from "./controllers/dashboard.js";
import {
    apiUserSearch,
    apiUserInfo,
    apiUpdateUser,
    apiGetNotebookContent,
    apiUpdateNotebookContent,
    apiOCR,
    apiRunner,
} from "./controllers/api.js";
import { renderEditUser } from "./controllers/user.js";

const router = express.Router();

router.get("/login", renderLogin);
router.post("/login", postLogin);

router.get("/dashboard", renderDashboard);

router.get("/api/:token/userSearch/:search", apiUserSearch);
router.get("/api/:token/userInfo/:uuid", apiUserInfo);
router.get(
    "/api/:token/updateUser/:uuid/:email/:username/:role",
    apiUpdateUser
);
router.get("/api/:token/notebook/:uuid", apiGetNotebookContent);
router.put("/api/:token/notebook/:uuid", apiUpdateNotebookContent);
router.get("/api/:token/ocr/notebook/:uuid/position/:position", apiOCR);
router.get("/api/:token/runner/notebook/:uuid/position/:position", apiRunner);

router.get("/edit/:uuid", renderEditUser);

export default router;
