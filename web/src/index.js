import express from "express";
import cors from "cors";
import router from "./router.js";
import cookieParser from "cookie-parser";
import path from "path";
import { fileURLToPath } from "url";
import { dirname } from "path";

const app = express();

const port = 3000;

app.use(express.urlencoded({ extended: true, limit: "50mb" }));
app.use(express.json({ limit: "50mb" }));
app.use(cors());
app.use(cookieParser());
console.log(path.join(dirname(fileURLToPath(import.meta.url)), "../js/"));
app.use(
    express.static(path.join(dirname(fileURLToPath(import.meta.url)), "../js/"))
);
app.set("views", "views");
app.set("view engine", "hbs");

app.use("/", router);

app.listen(port, () => {
    console.log(`Server listening on port ${port}.`);
});
