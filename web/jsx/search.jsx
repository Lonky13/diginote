const e = React.createElement;

class SearchEntry extends React.Component {
    constructor(props) {
        super(props);
        this.openEditor = this.openEditor.bind(this)
    }

    openEditor(event) {
        window.open(`/edit/${this.props.uuid}`,"_top")        
    }

    render() {
        return (
            <tr onClick={this.openEditor}>
                <th scope="row">{this.props.uuid}</th>
                <td>{this.props.username}</td>
                <td>{this.props.email}</td>
                <td>{this.props.role}</td>
            </tr>
        )
    }
}

class Search extends React.Component {
    constructor(props) {
        super(props);
        this.state = { search : "", users : [] };
        this.handleInputChange = this.handleInputChange.bind(this)
    }

    handleInputChange(event){
        let object = this
        this.state.search = event.target.value

        if(this.state.search){
            (async() => {
                object.state.users = []
                const response = await fetch(`/api/token/userSearch/${this.state.search}`);
                const data = await response.json()
                object.state.users = data
                object.forceUpdate();
            })();
        }else{
            this.forceUpdate();
        }
    }

    render() {
        let content;
        console.log(this.state.search)
        if(!this.state.search || this.state.users.length == 0){
            content = (
                <>
                    <div className="dropdown-divider"></div>
                    <div className="mt-4 mx-auto w-100 text-muted">
                        No users found.
                    </div>
                </>
            );
        }else{
            let users = [];

            for(let user of this.state.users){
                users.push(<SearchEntry key={user.id} uuid={user.id} username={user.username} email={user.email} role={user.role}/>);
            }

            content = (
                <table className="table table-bordered table-hover mb-0">
                    <tbody>
                        {users}
                    </tbody>
                </table>
            )
        }

        return (
            <>
                <input className="form-control mb-4" name="search" id="search" type="search" placeholder="Search" aria-label="Search" onChange={this.handleInputChange}/>
                {content}
            </>
        );
    }
}

const domContainer = document.querySelector("#search");
ReactDOM.render(e(Search), domContainer);
