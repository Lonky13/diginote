const e = React.createElement;

class EditUser extends React.Component {
    constructor(props){
        super(props);

        this.handleSubmit = this.handleSubmit.bind(this);
        this.changeEmail = this.changeEmail.bind(this);
        this.changeUsername = this.changeUsername.bind(this);
        this.changeRole = this.changeRole.bind(this);
        this.state = {email : '', username : '', uuid : null, role : null, errors : []}
    }

    async componentDidMount(){
        let uuid = window.location.href.split('/')[window.location.href.split('/').length - 1];
        const response = await fetch(`/api/token/userInfo/${uuid}`);
        const data = await response.json()

        this.state.uuid = data.id;
        this.state.role = Number(data.role);
        this.state.username = data.username;
        this.state.email = data.email;

        this.forceUpdate();
    }

    handleSubmit(event){
        (async() => {
            const response = await fetch(`/api/token/updateUser/${this.state.uuid}/${this.state.email}/${this.state.username}/${this.state.role}`);
            const data = await response.json();
            
            location.reload()
        })();
    }

    changeEmail(event){
        this.state.email = event.target.value;
    }
        
    changeUsername(event){
        this.state.username = event.target.value;
    }

    changeRole(event){
        this.state.role = event.target.value;
        this.forceUpdate();
    }
    
    render() {
        return (
            <>
            <input className="form-control mb-4" name="username" id="username" type="username" placeholder="Username" aria-label="Username" defaultValue={this.state.username} onChange={this.changeUsername}/>
            <input className="form-control mb-4" name="email" id="email" type="email" placeholder="Email" aria-label="Email" defaultValue={this.state.email} onChange={this.changeEmail} />
            <select value={this.state.role} className="form-control mb-4" name="role" id="role" aria-label="Role" onChange={this.changeRole}>
                <option value="0">0</option>
                <option value="1">1</option>
                <option value="2">2</option>
            </select>

            <button type="submit" className="btn btn-primary mb-2" onClick={this.handleSubmit}>Update</button>
            </>
        )
    }
}

const domContainer = document.querySelector("#edit_user");
ReactDOM.render(e(EditUser), domContainer);
