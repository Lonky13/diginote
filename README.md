# Upute za pokretanje

## 1. Linux

1. Koristeći package manager za Linux distribuciju koju koristite instalirajte `docker` i `docker-compose`.

    ```
    yay -S docker docker-compose   # primjer za Arch
    ```

2. Zatim pratite [korake](https://docs.docker.com/engine/install/linux-postinstall/) potrebne nakon instalacije dockera.

3. Klonirajte ovaj repozitorij.

    ```
    git clone https://gitlab.com/meisters/diginote.git
    ```
    
4. Pozicionirajte se u klonirani direktorij i pokrenite sljedeću naredbu kako bi se buildali i pokrenuli baza podataka, web/API, OCR i code runner.

    ```
    docker-compose --env-file .env -p diginote_dev -f docker-compose.yml up --build
    ```
    
5. Instalirajte `Android Studio Canary` koji se može preuzeti [ovdje](https://developer.android.com/studio/preview) i otvorite `diginote` projekt u njemu

6. U datoteci `android/gradle.properties` promijenite `DIGINOTE_DB_HOST` varijablu na odgovarajuću IP adresu vašeg računala koja se može dobaviti s `ip addr` naredbom.

7. Spojite mobitel s USB kabelom i stisnite `Run`.

## 2. Windows

Upute za Windows se razlikuju samo u načinu instalacije `docker` i `docker-compose` programa s obzirom da je za Windows potrebno instalirati i `Windows Subsystem for Linux`. Svi ostali koraci su isti.

## Administratorska kontrolna ploča

Produkcijska:

  > [http://54.164.72.14:3000/login](http://54.164.72.14:3000/login)

Lokalna:

  > [http://localhost:3000/login](http://localhost:3000/login)

## Aplikacija

Produkcijski build aplikacije može se naći [ovdje](https://gitlab.com/meisters/diginote/-/blob/master/android/app-debug.apk).

## Napomene

- Postoji testni obični korisnik:

    ```
    Username: test
    Email: test@test.com
    Password: testtest
    ```

- Postoji testni administrator najviše razine:

    ```
    Username: root
    Email: root@root.com
    Password: rootroot
    ```

- Prilikom prvog pokretanja OCR docker kontejnera nakon build faze, python skripta će skinuti OCR model što će vjerojatno trajati nekoliko minuta.

- Produkcijski server je pokrenut na free tier AWS EC2 instanci te je stoga komunikacija s bazom **znatno** sporija od lokalne verzije
